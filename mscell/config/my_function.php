<?php
// function_rupiah
function rupiah($angka){
	
	$hasil_rupiah = "Rp " . number_format($angka,0,',','.');
	return $hasil_rupiah;

}

// function_rupiah STYLE 2
function rupiah2($angka){
	
	$hasil_rupiah2 = "<span style='float:left'>Rp</span> <span style='float:right'>" . number_format($angka,0,',','.')."</span>";
	return $hasil_rupiah2;

}

	




//UNTUK MEMBERI WARNA PADA STATUS
		function status_pemesanan($isi_status){
			if($isi_status == 'Sudah Dipesan' ){
				return "<span style='color:blue;font-weight:bold'><center>".$isi_status."</center></span>";
			}
			if($isi_status == 'Sudah Dibatalkan' ){
				return "<span style='color:red;font-weight:bold'><center>".$isi_status."</center></span>";
			}
			if($isi_status == 'Sedang Dipesan' ){
				return "<span style='font-weight:bold'><center>".$isi_status."</center></span>";
			}
		}
		
		function warna_status($isi_status){
			if($isi_status == 'Sudah Bayar' ){
				return "<span style='color:green;font-weight:bold'><center>".$isi_status."</center></span>";
			}
			if($isi_status == 'Belum Bayar' ){
				return "<span style='color:orange;font-weight:bold'><center>".$isi_status."</center></span>";
			}
			if($isi_status == 'Sudah Lunas' ){
				return "<span style='color:blue;font-weight:bold'><center>".$isi_status."</center></span>";
			}
			if($isi_status == 'Sedang Diproses' ){
				return "<span style='font-weight:bold'><center>".$isi_status."</center></span>";
			}
			if($isi_status == 'Sedang Dikirim'){
				return "<span style='color:red;font-weight:bold'><center>".$isi_status."</center></span>";
			}
			if($isi_status == 'Telah Diterima' ){
				return "<span style='color:blue;font-weight:bold'><center>".$isi_status."</center></span>";
			}
		}
		//TERBILANG
function terbilang($i){
  $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  
  if ($i < 12) return " " . $huruf[$i];
  elseif ($i < 20) return terbilang($i - 10) . " belas";
  elseif ($i < 100) return terbilang($i / 10) . " puluh" . terbilang($i % 10);
  elseif ($i < 200) return " seratus" . terbilang($i - 100);
  elseif ($i < 1000) return terbilang($i / 100) . " ratus" . terbilang($i % 100);
  elseif ($i < 2000) return " seribu" . terbilang($i - 1000);
  elseif ($i < 1000000) return terbilang($i / 1000) . " ribu" . terbilang($i % 1000);
  elseif ($i < 1000000000) return terbilang($i / 1000000) . " juta" . terbilang($i % 1000000);    
}							
												

		//untuk membatasi kalimat yang tampil hanya 15 karakter
		function batasin1($isi){
			$a = substr($isi,0,90);
			$count = strlen($isi);
			if($count > 90){
				return $a." ...";
			}else{
				return $isi;
			}
			
		}

		function rpn_zero($isi_rpn){
			if($isi_rpn == '0' ){
				return " ";
			}else{
				return $isi_rpn;
			}
			
		}

		//untuk membatasi kalimat yang tampil hanya 15 karakter
		function batasin($isi){
			$a = substr($isi,0,35);
			$count = strlen($isi);
			if($count > 35){
				return $a." ...";
			}else{
				return $isi;
			}
			
		}

			//untuk datetime biar ngga nampil 0000-00-00 00:00:00
		function text_zero($isi_text){
			if($isi_text == '' ){
				return "<center>-</center>";
			}else{
				return $isi_text;
			}
			
		}
		
					//untuk datetime biar ngga nampil 0000-00-00 00:00:00
		function text_zero1($isi1){
			if($isi1 == '' ){
				return "<center>-</center>";
			}else{
				return $isi1;
			}
			
		}

							//untuk datetime biar ngga nampil 0000-00-00 00:00:00
		function approve_zero($isi1){
			if($isi1 == '' ){
				return "-";
			}else{
				return $isi1;
			}
			
		}

			//untuk datetime biar ngga nampil 0000-00-00 00:00:00
		function datetime_zero($isidatetime){
			if($isidatetime == '0000-00-00 00:00:00' ){
				return "<center>-</center>";
			}else{
				return "<center>".$isidatetime."</center>";
			}
			
		}

					//untuk datetime biar ngga nampil 0000-00-00 00:00:00
		function datetime_info($isi){
			if($isi == '0000-00-00 00:00:00' ){
				return "-";
			}else{
				return $isi;
			}
			
		}

				//untuk date aja biar ngga nampil 0000-00-00
		function tanggal_app($isi_date){
			if($isi_date == '0000-00-00' ){
				return " - ";
			}else{
				return $isi_date;
			}
			
		}
		//untuk date aja biar ngga nampil 0000-00-00
		function date_zero($isi_date){
			if($isi_date == '0000-00-00' ){
				return "<center>-</center>";
			}else{
				return "<center>".$isi_date."</center>";
			}
			
		}
		//UNTUK MEMBERI WARNA PADA STATUS
		

		function status_case_tl($tl, $q){
			if($tl == 'On Progress' && $q == ''){
				return "<span style='font-weight:bold'><i>".$tl."</i></span>";
			}
			if($tl == 'Approved' && $q == 'On Progress'){
				return "<span style='color:blue;font-weight:bold'><i>".$tl."</i></span>";
			}
			if($tl == '' && $q == 'On Progress'){
				return "<span style='color:red;font-weight:bold'><i>No Approval</i></span>";
			}

		}

		function status_case_q($tl, $q){
			if($tl == 'On Progress' && $q == ''){
				return "<span style='color:red;font-weight:bold'><i>Waiting for Team Leader Approval</i></span>";
			}
			if($tl == 'Approved' && $q == 'On Progress'){
				return "<span style='font-weight:bold'><i>".$q."</i></span>";
			}
			if($tl == '' && $q == 'On Progress'){
				return "<span style='font-weight:bold'><i>".$q."</i></span>";
			}

		}
		function view_perm_quality_case($a,$s){
			if(!$a) return;
			$a= explode(',',$a);
			if(!in_array($_SESSION['id_plant'],$a)) return;
			return $s;
		}

		function FlipDate($s,$type){
			switch($type){
			  case 'dmy':
				list($y,$m,$d) = explode('-',$s);
				return $d.'-'.$m.'-'.$y;
			  break;
			  case 'ymd':
				list($d,$m,$y) = explode('-',$s);
				return $y.'-'.$m.'-'.$d;
			  break;
			}
			return false;
		  }
	  
		  function conHari($hari){ 
			switch($hari){
			 case 'Sun':
			  $getHari = "Minggu";
			 break;
			 case 'Mon': 
			  $getHari = "Senin";
			 break;
			 case 'Tue':
			  $getHari = "Selasa";
			 break;
			 case 'Wed':
			  $getHari = "Rabu";
			 break;
			 case 'Thu':
			  $getHari = "Kamis";
			 break;
			 case 'Fri':
			  $getHari = "Jumat";
			 break;
			 case 'Sat':
			  $getHari = "Sabtu";
			 break;
			 default:
			  $getHari = "Salah"; 
			 break;
			}
			
			return $getHari;
		   }

		   function sudah_login(){
			if(!@$_SESSION['sudah_login']){
				return false;
			}
			return true;
	   }
?>