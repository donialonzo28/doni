-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 26, 2019 at 11:28 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `griya-muslim`
--

-- --------------------------------------------------------

--
-- Table structure for table `baju`
--

CREATE TABLE IF NOT EXISTS `baju` (
  `kd_baju` char(5) NOT NULL,
  `nama_baju` char(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `nama_kategori` char(50) NOT NULL,
  `harga` float NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `baju`
--

INSERT INTO `baju` (`kd_baju`, `nama_baju`, `deskripsi`, `nama_kategori`, `harga`, `foto`) VALUES
('B01', 'Gamis Kebaya Tanamo', 'Gamis Kebaya Tanamo bagus banget murah asli real foto bagus banget beneran ini murah banget', 'Gamis', 120000, 'Gamis Kebaya Tanamo.jpg'),
('B02', 'Gamis Najibah Syari', 'Gamis Najibah Syari Bagus asli original bahan adem', 'Gamis', 150000, 'Gamis Najibah Syari.jpg'),
('B03', 'Gamis Najma Maxi', 'Gamis Najibah Maxi Bagus asli original bahan adem', 'Gamis', 140000, 'Gamis Najma Maxi.jpg'),
('B04', 'Gamis Raline Maxi', 'Gamis Raline Maxi Bagus asli original bahan agak adem', 'Gamis', 140000, 'Gamis Raline Maxi.jpg'),
('B05', 'Gamis Syari Salma Syari Dress Muslim', 'Gamis Syari Salma Syari Dress Muslim Bagus asli original bahan agak lumayan adem', 'Gamis', 140000, 'Gamis Syari Salma Syari Dress Muslim.jpg'),
('B06', 'Abaya Dubai Heidy', 'Abaya Dubai Heidy asli dari dubai bukan aspal tapi agak beneran asli ini bener', 'Abaya', 635000, 'Abaya Dubai Heidy.jpg'),
('B07', 'Azkiya Drees Muslim Abaya', 'Azkiya Drees Muslim Abaya bahan beneran agak adem nggak panas', 'Abaya', 245000, 'Azkiya Drees Muslim Abaya.jpg'),
('B08', 'Abaya Maxi Dress Arab Saudi Bordir Zephy Turki', 'Abaya Maxi Dress Arab Saudi Bordir Zephy Turki langsung import dari luar negeri pakai pesawat', 'Abaya', 425000, 'Abaya Maxi Dress Arab Saudi Bordir Zephy Turki.jpg'),
('B09', 'Mukena Bordir Aleena', 'Mukena Bordir Aleena langsung bisa dipakai buat sholat isya', 'Mukena', 195000, 'Mukena Bordir Aleena.jpg'),
('B10', 'Mukena Nadira TSP', 'Mukena Nadira TSP bahan adem harga agak murah berkualitas', 'Mukena', 105000, 'Mukena Nadira TSP.jpg'),
('B11', 'Mukena Renda Pouch Traveling', 'Nggak ada alasan lagi ninggalin sholat saat traveling kini tersedia Mukena Renda Pouch Traveling', 'Mukena', 212000, 'Mukena Renda Pouch Traveling.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `jasa_pengiriman`
--

CREATE TABLE IF NOT EXISTS `jasa_pengiriman` (
  `id_perusahaan` int(10) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jasa_pengiriman`
--

INSERT INTO `jasa_pengiriman` (`id_perusahaan`, `nama_perusahaan`) VALUES
(1, 'JNE'),
(2, 'TIKI'),
(3, 'POS EKSPRESS');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
  `id` int(2) NOT NULL,
  `nama_kategori` char(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `nama_kategori`) VALUES
(1, 'Gamis'),
(2, 'Abaya'),
(3, 'Mukena');

-- --------------------------------------------------------

--
-- Table structure for table `kota`
--

CREATE TABLE IF NOT EXISTS `kota` (
  `id_kota` int(3) NOT NULL,
  `nama_kota` varchar(100) NOT NULL,
  `ongkos_kirim` int(10) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kota`
--

INSERT INTO `kota` (`id_kota`, `nama_kota`, `ongkos_kirim`) VALUES
(1, 'Jakarta', 15000),
(2, 'Bandung', 15000),
(3, 'Surabaya', 13000),
(4, 'Semarang', 17500),
(5, 'Medan', 20000),
(6, 'Aceh', 25000),
(7, 'Banjarmasin', 17500);

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE IF NOT EXISTS `pembayaran` (
  `no_pembayaran` int(3) NOT NULL,
  `no_pemesanan` char(5) NOT NULL,
  `tanggal_pembayaran` datetime DEFAULT NULL,
  `foto` char(200) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pemesanan_detail`
--

CREATE TABLE IF NOT EXISTS `pemesanan_detail` (
  `no_pemesanan` char(5) CHARACTER SET latin1 NOT NULL,
  `kd_baju` char(5) CHARACTER SET latin1 NOT NULL,
  `ukuran_baju` char(20) COLLATE latin1_general_ci NOT NULL,
  `jumlah_item` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pemesanan_header`
--

CREATE TABLE IF NOT EXISTS `pemesanan_header` (
  `no_pemesanan` char(5) NOT NULL,
  `kd_user` char(4) NOT NULL,
  `status_pemesanan` char(20) NOT NULL,
  `tanggal_pemesanan` datetime NOT NULL,
  `id_kota` int(3) NOT NULL,
  `total_bayar` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pemesanan_temp`
--

CREATE TABLE IF NOT EXISTS `pemesanan_temp` (
  `id_temp` int(5) NOT NULL,
  `kd_baju` char(5) CHARACTER SET latin1 NOT NULL,
  `ukuran_baju` char(20) COLLATE latin1_general_ci NOT NULL,
  `kd_user` char(4) CHARACTER SET latin1 NOT NULL,
  `jumlah_item` int(3) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=86 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengiriman`
--

CREATE TABLE IF NOT EXISTS `pengiriman` (
  `no_pengiriman` char(5) NOT NULL,
  `tgl_pengiriman` date DEFAULT NULL,
  `kd_pembayaran` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `kd_user` char(4) NOT NULL,
  `username` char(20) NOT NULL,
  `password` char(200) NOT NULL,
  `nama` char(50) NOT NULL,
  `alamat` char(200) NOT NULL,
  `no_hp` char(15) NOT NULL,
  `level` char(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`kd_user`, `username`, `password`, `nama`, `alamat`, `no_hp`, `level`) VALUES
('U01', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Administrator', '-', '-', 'Admin'),
('U02', 'konsumen', '94727b16c2221c188d39993e39f39ac3', 'konsumen', 'tes', '123', 'Pelanggan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `baju`
--
ALTER TABLE `baju`
  ADD PRIMARY KEY (`kd_baju`);

--
-- Indexes for table `jasa_pengiriman`
--
ALTER TABLE `jasa_pengiriman`
  ADD PRIMARY KEY (`id_perusahaan`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kota`
--
ALTER TABLE `kota`
  ADD PRIMARY KEY (`id_kota`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`no_pembayaran`);

--
-- Indexes for table `pemesanan_header`
--
ALTER TABLE `pemesanan_header`
  ADD PRIMARY KEY (`no_pemesanan`);

--
-- Indexes for table `pemesanan_temp`
--
ALTER TABLE `pemesanan_temp`
  ADD PRIMARY KEY (`id_temp`);

--
-- Indexes for table `pengiriman`
--
ALTER TABLE `pengiriman`
  ADD PRIMARY KEY (`no_pengiriman`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`kd_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jasa_pengiriman`
--
ALTER TABLE `jasa_pengiriman`
  MODIFY `id_perusahaan` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `kota`
--
ALTER TABLE `kota`
  MODIFY `id_kota` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `no_pembayaran` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pemesanan_temp`
--
ALTER TABLE `pemesanan_temp`
  MODIFY `id_temp` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=86;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
