-- MariaDB dump 10.17  Distrib 10.4.14-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: mscell
-- ------------------------------------------------------
-- Server version	5.6.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `baju`
--

DROP TABLE IF EXISTS `baju`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `baju` (
  `kd_baju` char(5) NOT NULL,
  `nama_baju` char(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `nama_kategori` char(50) NOT NULL,
  `harga` float NOT NULL,
  `foto` varchar(100) NOT NULL,
  PRIMARY KEY (`kd_baju`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `baju`
--

LOCK TABLES `baju` WRITE;
/*!40000 ALTER TABLE `baju` DISABLE KEYS */;
INSERT INTO `baju` VALUES ('B01','Gamis Kebaya Tanamo','Gamis Kebaya Tanamo bagus banget murah asli real foto bagus banget beneran ini murah banget','Gamis',120000,'Gamis Kebaya Tanamo.jpg'),('B02','Gamis Najibah Syari','Gamis Najibah Syari Bagus asli original bahan adem','Gamis',150000,'Gamis Najibah Syari.jpg'),('B03','Gamis Najma Maxi','Gamis Najibah Maxi Bagus asli original bahan adem','Gamis',140000,'Gamis Najma Maxi.jpg'),('B04','Gamis Raline Maxi','Gamis Raline Maxi Bagus asli original bahan agak adem','Gamis',140000,'Gamis Raline Maxi.jpg'),('B05','Gamis Syari Salma Syari Dress Muslim','Gamis Syari Salma Syari Dress Muslim Bagus asli original bahan agak lumayan adem','Gamis',140000,'Gamis Syari Salma Syari Dress Muslim.jpg'),('B06','Abaya Dubai Heidy','Abaya Dubai Heidy asli dari dubai bukan aspal tapi agak beneran asli ini bener','Abaya',635000,'Abaya Dubai Heidy.jpg'),('B07','Azkiya Drees Muslim Abaya','Azkiya Drees Muslim Abaya bahan beneran agak adem nggak panas','Abaya',245000,'Azkiya Drees Muslim Abaya.jpg'),('B08','Abaya Maxi Dress Arab Saudi Bordir Zephy Turki','Abaya Maxi Dress Arab Saudi Bordir Zephy Turki langsung import dari luar negeri pakai pesawat','Abaya',425000,'Abaya Maxi Dress Arab Saudi Bordir Zephy Turki.jpg'),('B09','Mukena Bordir Aleena','Mukena Bordir Aleena langsung bisa dipakai buat sholat isya','Mukena',195000,'Mukena Bordir Aleena.jpg'),('B10','Mukena Nadira TSP','Mukena Nadira TSP bahan adem harga agak murah berkualitas','Mukena',105000,'Mukena Nadira TSP.jpg'),('B11','Mukena Renda Pouch Traveling','Nggak ada alasan lagi ninggalin sholat saat traveling kini tersedia Mukena Renda Pouch Traveling','Mukena',212000,'Mukena Renda Pouch Traveling.jpg');
/*!40000 ALTER TABLE `baju` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `barang`
--

DROP TABLE IF EXISTS `barang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barang` (
  `kd_barang` char(5) NOT NULL,
  `nama_barang` char(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `id_kategori` int(2) NOT NULL,
  `harga` float NOT NULL,
  `foto` varchar(100) NOT NULL,
  PRIMARY KEY (`kd_barang`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barang`
--

LOCK TABLES `barang` WRITE;
/*!40000 ALTER TABLE `barang` DISABLE KEYS */;
INSERT INTO `barang` VALUES ('B01','SAMSUNG GALAXY S8 GOLD 4/64 EX SEIN','Deskripsi SAMSUNG GALAXY S8 GOLD 4/64 EX SEIN\r\n\r\nFS :\r\n\r\nSamsung Galaxy S8 Ex Sein Mapple Gold\r\n\r\nMohon Dibaca Dengan Baik Baik\r\n\r\nKondisi :\r\n\r\n+ Sensor Semua Berfungsi\r\n+ Kamera Depan Belakang Berfungsi Baik\r\n+ Touchscreen Aman \r\n+ Layar Tidak Ada Retak\r\n+ Body Mulus \r\n+ Baterai Normal\r\n\r\n- Layar Tidak Ada Shadow (Cek Foto Terakhir)\r\n- Layar Suka Berubah Warna Hijau Tapi Suka Berubah Jadi Bening (Cek Foto Terakhir)\r\n- LCD Green Screen Apabila Kontras Dibawah 50% (Bisa diakali dengan Applikasi Oled Saver)\r\n- AOD Harus Aktif Kalau Ga Aktif Agak Susah Dinyalainnya\r\n\r\nKelengkapan :\r\n\r\n- Unit S8\r\n- Dus Book',1,1299000,'5045f55f-09b0-415f-9f96-51a51a2d89e0.jpg');
/*!40000 ALTER TABLE `barang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jasa_pengiriman`
--

DROP TABLE IF EXISTS `jasa_pengiriman`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jasa_pengiriman` (
  `id_perusahaan` int(10) NOT NULL AUTO_INCREMENT,
  `nama_perusahaan` varchar(100) NOT NULL,
  PRIMARY KEY (`id_perusahaan`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jasa_pengiriman`
--

LOCK TABLES `jasa_pengiriman` WRITE;
/*!40000 ALTER TABLE `jasa_pengiriman` DISABLE KEYS */;
INSERT INTO `jasa_pengiriman` VALUES (1,'JNE'),(2,'TIKI'),(3,'POS EKSPRESS');
/*!40000 ALTER TABLE `jasa_pengiriman` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategori`
--

DROP TABLE IF EXISTS `kategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategori` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `nama_kategori` char(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategori`
--

LOCK TABLES `kategori` WRITE;
/*!40000 ALTER TABLE `kategori` DISABLE KEYS */;
INSERT INTO `kategori` VALUES (1,'Smartphone'),(2,'Power Adapter'),(3,'Power Bank'),(4,'Earphone'),(5,'Case'),(6,'Charger Kabel Data'),(7,'Accesories');
/*!40000 ALTER TABLE `kategori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kota`
--

DROP TABLE IF EXISTS `kota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kota` (
  `id_kota` int(3) NOT NULL AUTO_INCREMENT,
  `nama_kota` varchar(100) NOT NULL,
  `ongkos_kirim` int(10) NOT NULL,
  PRIMARY KEY (`id_kota`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kota`
--

LOCK TABLES `kota` WRITE;
/*!40000 ALTER TABLE `kota` DISABLE KEYS */;
INSERT INTO `kota` VALUES (1,'Jakarta',15000),(2,'Bandung',15000),(3,'Surabaya',13000),(4,'Semarang',17500),(5,'Medan',20000),(6,'Aceh',25000),(7,'Banjarmasin',17500);
/*!40000 ALTER TABLE `kota` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pembayaran`
--

DROP TABLE IF EXISTS `pembayaran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pembayaran` (
  `no_pembayaran` int(3) NOT NULL AUTO_INCREMENT,
  `no_pemesanan` char(5) NOT NULL,
  `tanggal_pembayaran` datetime DEFAULT NULL,
  `foto` char(200) NOT NULL,
  PRIMARY KEY (`no_pembayaran`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pembayaran`
--

LOCK TABLES `pembayaran` WRITE;
/*!40000 ALTER TABLE `pembayaran` DISABLE KEYS */;
INSERT INTO `pembayaran` VALUES (7,'P02','2020-11-20 16:07:06','5045f55f-09b0-415f-9f96-51a51a2d89e0.jpg');
/*!40000 ALTER TABLE `pembayaran` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pemesanan_detail`
--

DROP TABLE IF EXISTS `pemesanan_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pemesanan_detail` (
  `no_pemesanan` char(5) CHARACTER SET latin1 NOT NULL,
  `kd_barang` char(5) CHARACTER SET latin1 NOT NULL,
  `ukuran_baju` char(20) COLLATE latin1_general_ci NOT NULL,
  `jumlah_item` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pemesanan_detail`
--

LOCK TABLES `pemesanan_detail` WRITE;
/*!40000 ALTER TABLE `pemesanan_detail` DISABLE KEYS */;
INSERT INTO `pemesanan_detail` VALUES ('P02','B01','',1);
/*!40000 ALTER TABLE `pemesanan_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pemesanan_header`
--

DROP TABLE IF EXISTS `pemesanan_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pemesanan_header` (
  `no_pemesanan` char(5) NOT NULL,
  `kd_user` char(4) NOT NULL,
  `status_pemesanan` char(20) NOT NULL,
  `tanggal_pemesanan` datetime NOT NULL,
  `id_kota` int(3) NOT NULL,
  `total_bayar` float NOT NULL,
  PRIMARY KEY (`no_pemesanan`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pemesanan_header`
--

LOCK TABLES `pemesanan_header` WRITE;
/*!40000 ALTER TABLE `pemesanan_header` DISABLE KEYS */;
INSERT INTO `pemesanan_header` VALUES ('P01','U03','Sedang Diproses','2020-11-20 16:03:39',2,1314000),('P02','U03','Telah Diterima','2020-11-20 16:05:00',2,1314000);
/*!40000 ALTER TABLE `pemesanan_header` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pemesanan_temp`
--

DROP TABLE IF EXISTS `pemesanan_temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pemesanan_temp` (
  `id_temp` int(5) NOT NULL AUTO_INCREMENT,
  `kd_barang` char(5) CHARACTER SET latin1 NOT NULL,
  `ukuran_baju` char(20) COLLATE latin1_general_ci NOT NULL,
  `kd_user` char(4) CHARACTER SET latin1 NOT NULL,
  `jumlah_item` int(3) NOT NULL,
  PRIMARY KEY (`id_temp`)
) ENGINE=MyISAM AUTO_INCREMENT=88 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pemesanan_temp`
--

LOCK TABLES `pemesanan_temp` WRITE;
/*!40000 ALTER TABLE `pemesanan_temp` DISABLE KEYS */;
/*!40000 ALTER TABLE `pemesanan_temp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pengiriman`
--

DROP TABLE IF EXISTS `pengiriman`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pengiriman` (
  `no_pengiriman` char(5) NOT NULL,
  `tgl_pengiriman` date DEFAULT NULL,
  `kd_pembayaran` int(3) NOT NULL,
  PRIMARY KEY (`no_pengiriman`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pengiriman`
--

LOCK TABLES `pengiriman` WRITE;
/*!40000 ALTER TABLE `pengiriman` DISABLE KEYS */;
INSERT INTO `pengiriman` VALUES ('PN01','2020-11-20',7);
/*!40000 ALTER TABLE `pengiriman` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `kd_user` char(4) NOT NULL,
  `username` char(20) NOT NULL,
  `password` char(200) NOT NULL,
  `nama` char(50) NOT NULL,
  `alamat` char(200) NOT NULL,
  `no_hp` char(15) NOT NULL,
  `level` char(20) NOT NULL,
  PRIMARY KEY (`kd_user`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('U01','admin','21232f297a57a5a743894a0e4a801fc3','Administrator','-','-','Admin'),('U02','konsumen','94727b16c2221c188d39993e39f39ac3','konsumen','tes','123','Pelanggan'),('U03','username','5f4dcc3b5aa765d61d8327deb882cf99','Nama Lengkap','Lorem ipsum dolor sit amet','0838','Pelanggan');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'mscell'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-20 16:17:44
