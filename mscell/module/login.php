<div class="offcanvas-wrapper">
      <!-- Page Title-->
      <div class="page-title">
        <div class="container">
          <div class="column">
            <h1>Login / Registrasi Akun</h1>
          </div>
          <div class="column">
            <ul class="breadcrumbs">
              <li><a href="index.html">MS Cell</a>
              </li>
              <li class="separator">&nbsp;</li>
              <li><a href="#">Akun</a>
              </li>
              <li class="separator">&nbsp;</li>
              <li>Login / Registrasi</li>
            </ul>
          </div>
        </div>
      </div>
      <!-- Page Content-->
      <div class="container padding-bottom-3x mb-2">
        <div class="row">
          <div class="col-md-6">
            <form action="logs.php?op=in" class="login-box" method="post">
              <h4 class="margin-bottom-1x">Silahkan login disini</h4>
              <div class="form-group input-group">
                <input name="login_username" class="form-control" type="text" placeholder="Username" required><span class="input-group-addon"><i class="icon-mail"></i></span>
              </div>
              <div class="form-group input-group">
                <input name="login_password" class="form-control" type="password" placeholder="Password" required><span class="input-group-addon"><i class="icon-lock"></i></span>
              </div>
              <div class="d-flex flex-wrap justify-content-between padding-bottom-1x">
                <div class="custom-control custom-checkbox">
                </div><a class="navi-link" href="account-password-recovery.html">Forgot password?</a>
              </div>
              <div class="text-center text-sm-right">
                <button class="btn btn-primary margin-bottom-none" type="submit">Login</button>
              </div>
            </form>
          </div>
          <div class="col-md-6">
            <div class="padding-top-3x hidden-md-up"></div>
            <h3 class="margin-bottom-1x">Belum punya akun, Daftar Gratis disini</h3>
            <p>Pendaftaran membutuhkan waktu kurang dari satu menit, tetapi memberi Anda kontrol penuh atas pesanan Anda.</p>
            <form action="logs.php?op=reg" class="row" method="post">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="reg-fn">Nama Lengkap</label>
                  <input name="nama" class="form-control" type="text"  id="reg-fn" required>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="reg-ln">Username</label>
                  <input name="username" class="form-control" type="text" id="reg-fn" required>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="reg-fn">Alamat</label>
                  <input name="alamat" class="form-control" type="text" id="reg-fn" required>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="reg-phone">Nomor Hp</label>
                  <input name="no_hp" class="form-control" type="text" id="reg-phone" required>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="reg-pass">Password</label>
                  <input name="password" class="form-control" type="password" id="reg-pass" required>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="reg-pass-confirm">Konfirmasi Password</label>
                  <input name="konfirmasi_password" class="form-control" type="password" id="reg-pass-confirm" required>
                </div>
              </div>
              <div class="col-12 text-center text-sm-right">
                <button name="register" class="btn btn-primary margin-bottom-none" type="submit">Daftar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>