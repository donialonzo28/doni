<div class="offcanvas-wrapper">
      <!-- Page Title-->
      <div class="page-title">
        <div class="container">
          <div class="column">
            <h1>Cara Pembelian</h1>
          </div>
          <div class="column">
            <ul class="breadcrumbs">
              <li><a href="index.html">MS Cell</a>
              </li>
              <li class="separator">&nbsp;</li>
              <li>Cara Beli</li>
            </ul>
          </div>
        </div>
      </div>
      <!-- Page Content-->
      <div class="container padding-bottom-3x mb-2">
        <div class="row justify-content-center">
          <!-- Content-->
          <div class="col-lg-10">
            <h2 class="padding-top-2x">Cara Pembelian di MS Cell</h2>
<p>1.) Klik pada tombol Beli pada produk yang ingin Anda pesan.<br>
2.) Produk yang Anda pesan akan masuk ke dalam Keranjang Belanja. Anda dapat melakukan perubahan jumlah produk yang diinginkan dengan mengganti angka di kolom Jumlah. Sedangkan untuk menghapus sebuah produk dari Keranjang Belanja, klik tombol hapus yang berada di kolom paling kanan.</br>
3.) Jika sudah selesai, klik tombol Selesai Belanja, maka akan tampil form untuk pengisian data kustomer/pembeli.</br>
4.) Setelah data pembeli selesai diisikan, klik tombol Proses, maka akan tampil data pembeli beserta produk yang dipesannya (jika diperlukan catat nomor pemesanannta).</br>
5.) Apabila telah melakukan pembayaran, maka produk/barang akan segera kami kirimkan. </p>
			<div class="single-post-footer">
              <div class="column"><a class="sp-tag" href="#">&nbsp;#Kota_Cilegon,</a><a class="sp-tag" href="#">&nbsp;#Toko_Online</a></div>
              
            </div>
          </div>
        </div>
      </div>
      <!-- Site Footer-->
    </div>