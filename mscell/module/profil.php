<div class="offcanvas-wrapper">
      <!-- Page Title-->
      <div class="page-title">
        <div class="container">
          <div class="column">
            <h1>Profil</h1>
          </div>
          <div class="column">
            <ul class="breadcrumbs">
              <li><a href="index.html">MS Cell</a>
              </li>

              <li class="separator">&nbsp;</li>
              <li>Profil</li>
            </ul>
          </div>
        </div>
      </div>
      <!-- Page Content-->
      <div class="container padding-bottom-3x mb-2">
        <div class="row justify-content-center">
          <!-- Content-->
          <div class="col-lg-10">
            <h2 class="padding-top-2x">MS Cell</h2>
            <p>MS Cell adalah toko online, yang menyediakan berbagai macam jenis barang yang bisa anda dapatkan. MS Cell ingin memberikan kemudahan kepada para calon pembeli, cara santai, mudah dan hemat dalam berbelanja, serta berkualias dengan harga yang terjangkau.
			Karena itulah website ini dibuat sedemikian sederhananya sehingga diharapkan dapat membantu para pengunjung untuk dapat menelusuri produk-produk yang ditawarkan secara lebih mudah.
			Selain melayani pesanan produk-produk yang ada di toko online, kami menerima pembelian secara offline dengan datang ke toko kami yang ada terletak di kota cilegon.
			Akhirnya, kami mengucapkan terima kasih atas kunjungan anda di MS Cell.</p> <div class="single-post-footer">
              <div class="column"><a class="sp-tag" href="#">&nbsp;#Kota_Cilegon,</a><a class="sp-tag" href="#">&nbsp;#Toko_Online</a></div>
              
            </div>
          </div>
        </div>
      </div>
      <!-- Site Footer-->
    </div>