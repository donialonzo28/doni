 <div class="offcanvas-wrapper">
      <!-- Page Title-->
      <div class="page-title">
        <div class="container">
          <div class="column">
            <h1>Data Produk</h1>
          </div>
          <div class="column">
            <ul class="breadcrumbs">
              <li><a href="index.html">MS Cell</a>
              </li>
              <li class="separator">&nbsp;</li>
              <li>Data-Produk</li>
            </ul>
          </div>
        </div>
      </div>
      <!-- Page Content-->
      <div class="container padding-bottom-3x mb-1">
        <div class="row">
          <!-- Products-->
          <div class="col-xl-9 col-lg-8 order-lg-2">
            <!-- Shop Toolbar-->
            <div class="shop-toolbar padding-bottom-1x mb-2">
              <div class="column">
              <div class="shop-sorting">
                  <label for="sorting">kategori Berdasarkan:</label>
                  <select onChange="window.document.location.href=this.options[this.selectedIndex].value;" class="form-control" id="kategori">
                 <option value="#">Pilih Kategori </option>
                  <?php
                  $qry_kategori=mysql_query("SELECT * FROM `kategori` ORDER BY id ASC");
                  while ($kategori=mysql_fetch_array($qry_kategori)){
                  ?>
                    <option value="?page=Produk-2&kategori=<?php echo $kategori['id_kategori'] ?>" <?php (@$_GET['kategori']== $kategori['id']) ? $sl = 'selected' : $sl =''; echo $sl ?>><?php echo $kategori['nama_kategori'] ?></option>
                    <?php } ?>
                   </select>
                  
                </div>
              </div>
              <div class="column">
                <div class="shop-view">
				<a class="grid-view" href="?page=Produk"><span></span><span></span><span></span></a>
				<a class="list-view active" href="?page=Produk"><span></span><span></span><span></span></a></div>
              </div>
            </div>
		  
            <?php

if(!@$_GET['kategori']){
  @$dt_kategori = "";
  // menejemen pagination untuk url sesuai kondisi diatas
  $select_dt_kategori = "?page=Produk-2&halaman=";

} 
// Kondisi where untuk menkategori berdasarkan nama
if(@$_GET['kategori']){
    @$dt_kategori = "WHERE id_kategori='".@$_GET['kategori']."'";

  // menejemen pagination untuk url sesuai kondisi diatas
  $select_dt_kategori ='?page=Produk-2&kategori='.@$_GET['kategori'].'&halaman=';
}
    //LIMIT HALAMAN
		$page = (isset($_GET['halaman']))? $_GET['halaman'] : 1;
    $limit = 6; 
    $limit_start = ($page - 1) * $limit;

		$qry=mysql_query("SELECT * FROM `barang` $dt_kategori LIMIT ".$limit_start.",".$limit);
		while ($tampil=mysql_fetch_array($qry)){ ?>


            <!-- Product-->
            <div class="product-card product-list"><a class="product-thumb" href="?page=Detail-Produk&id=<?php echo $tampil['kd_barang'] ?>">
                  <!-- <div class="rating-stars"><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i>
                  </div> -->
                  <img src="foto_barang/<?php echo $tampil['foto'] ?>" alt="Product"></a>
              <div class="product-info">
                <h3 class="product-title"><a href="?page=Detail-Produk&id=<?php echo $tampil['kd_barang'] ?>"><?php echo $tampil['nama_barang'] ?></a></h3>
                <h4 class="product-price"><?php echo rupiah($tampil['harga']) ?></h4>
                <p class="hidden-xs-down"><?php echo $tampil['deskripsi'] ?></p>
                <div class="product-buttons">
                <a class="btn btn-outline-primary btn-sm"
                href="proses/tambah_item_keranjang.php?id=<?php echo $tampil['kd_barang']; ?>">Tambahkan Ke Keranjang</a>
               </div>
              </div>
            </div>
            <?php } ?>
            <div class="pt-2">
              <!-- Pagination-->
              <nav class="pagination">
                <div class="column">
                  <ul class="pages">
                  <?php
													// Buat query untuk menghitung semua jumlah data
		$sql2 = mysql_query("SELECT COUNT(*) AS jumlah from `barang` $dt_kategori");
		$get_jumlah = mysql_fetch_array($sql2);

		$jumlah_page = ceil($get_jumlah['jumlah'] / $limit); // Hitung jumlah halamannya
		$jumlah_number = 6; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
		$start_number = ($page > $jumlah_number)? $page - $jumlah_number : 1; // Untuk awal link number
		$end_number = ($page < ($jumlah_page - $jumlah_number))? $page + $jumlah_number : $jumlah_page; // Untuk akhir link number

    for($i = $start_number; $i <= $end_number; $i++)
    {

        $link_active = ($page == $i)? ' class="active"' : '';
    ?>
          <li <?php echo $link_active; ?>><a href="<?php echo $select_dt_kategori; echo $i; ?>"><?php echo $i; ?></a></li>
    <?php
    }
    ?>
                </ul>
              </div>

              <!-- LINK NEXT  -->
	<?php
		// Jika page sama dengan jumlah page, maka disable link NEXT nya
		// Artinya page tersebut adalah page terakhir 
		if($page == $jumlah_page){ // Jika page terakhir
	?>
		<div class="column text-right hidden-xs-down"><a onclick="return alert('Ini sudah halaman terakhir')" class="btn btn-outline-secondary btn-sm" href="#">Next&nbsp;<i class="icon-arrow-right"></i></a></div>
	<?php
		}else{ // Jika Bukan page terakhir
			$link_next = ($page < $jumlah_page)? $page + 1 : $jumlah_page;
	?>
  <div class="column text-right hidden-xs-down"><a class="btn btn-outline-secondary btn-sm" href="<?php echo $select_dt_kategori; echo $link_next; ?>">Next&nbsp;<i class="icon-arrow-right"></i></a></div>

	<?php
		}	
		?>
    </nav>
            </div>
          </div>
          <!-- Sidebar          -->
          <div class="col-xl-3 col-lg-4 order-lg-1">
            <button class="sidebar-toggle position-left" data-toggle="modal" data-target="#modalShopFilters"><i class="icon-layout"></i></button>
            <aside class="sidebar sidebar-offcanvas">
              <!-- Widget Categories-->
              <section class="widget widget-categories">
                <h3 class="widget-title">Kategori Produk</h3>
                <ul>
                <!-- BATAS -->
            <?php
            //KATEGORI OTOMATIS
            $qry_kategori=mysql_query("SELECT * FROM `kategori` ORDER BY id ASC");
            while ($kategori=mysql_fetch_array($qry_kategori)){
            $id_kategori =  $kategori['id'];
            $nama_kategori =  $kategori['nama_kategori'];

                  $qry1= mysql_query("SELECT COUNT(*) jumlah FROM barang where id_kategori='".$id_kategori."'");
                  $dt_jumlah = mysql_fetch_array($qry1);
            ?>
                  <li class="has-children expanded"><a href="#"><?php echo $nama_kategori ?></a><span>(<?php echo $dt_jumlah['jumlah'] ?>)</span>
                    <ul>
                    <?php
		
		$qry=mysql_query("SELECT * FROM `barang`
    WHERE id_kategori='".$id_kategori."'");
		while ($tampil=mysql_fetch_array($qry)){ ?>
                      <li><a href="?page=Detail-Produk&id=<?php echo $tampil['kd_barang'] ?>"><?php echo $tampil['nama_barang'] ?></a>
                      </li>
    <?php } ?>
                    </ul>
                  </li>
    <?php } //END KATEGORI OTOMATIS?>

                  <!-- BATAS -->
                </ul>
              </section>
              <!-- Widget Price Range-->
              
              <!-- Widget Brand Filter-->
              
              <!-- Widget Size Filter-->
             
              <!-- Promo Banner-->
              
            </aside>
          </div>
        </div>
      </div>
      <!-- Site Footer-->
      
    </div>
	
	