<?php
  if(!sudah_login()){ //Jika belum login maka ya login dulu ya
    echo "<script> alert('Maaf, silahkan login terlebih dahulu');window.location.href='index.php?page=Login'</script>";
  }
  ?>
<div class="offcanvas-wrapper">
      <!-- Page Title-->
      <div class="page-title">
        <div class="container">
          <div class="column">
            <h1>Data Pemesanan</h1>
          </div>
          <div class="column">
            <ul class="breadcrumbs">
              <li><a href="index.html">MS Cell</a>
              </li>
              <li class="separator">&nbsp;</li>
              <li>Data Pemesanan</li>
            </ul>
          </div>
        </div>
      </div>
      <!-- Page Content-->
      <div class="container padding-bottom-1x mb-2">
        <div class="row">
          <!-- Checkout Adress-->
          <div class="col-xl-12 col-lg-8">
            <h4>List Data Pemesanan</h4>
            <hr class="padding-bottom-1x">
			
			<table width="100%" border="0">
			<tbody><tr>
				<td width="135">
				<form action="" method="GET">
				<div class="input-group form">
	            	<input type="hidden" name="page" value="Pemesanan">
					<input value="<?php echo @$_GET['cari']; ?>" class="form-control form-control-rounded" name="cari" type="text" id="normal-rounded-input" value="" placeholder="Pencarian">
	                </div></td>
					<td width="340">
					 <button class="btn btn-rounded btn-primary" type="submit">Cari</button>
					 </form>
           </td>
		  	</tr>
		  	<tr>
		    	<td>&nbsp;</td>
		    	<td align="right">&nbsp;</td>
	      	</tr>
		</tbody></table>
			
			<div class="table-responsive">
              <table class="table table-hover table-bordered">
                <thead style="background-color: #f73859;">
                  <tr class="text-white">
                    <th class="text-center">No Pemesanan</th>
										      <th class="text-center">Tanggal Pesan</th>
                          <th class="text-center">Nama Konsumen</th>
										      <th class="text-center">Jumlah Produk</th>
										      <th class="text-center">Jumlah Barang</th>
										      <th class="text-center">ongkir</th>
										      <th class="text-center">Total Bayar</th>
										      <th class="text-center">Status</th>
										      <th class="text-center">Aksi</th>
                  </tr>
                </thead>
                <?php
			
      //LIMIT HALAMAN
      $page = (isset($_GET['halaman']))? $_GET['halaman'] : 1;
      $limit = 6; 
      $limit_start = ($page - 1) * $limit;
      $user=$_SESSION['kd_user'];
  
  if(!@$_GET['cari']){
      @$dt_cari = "WHERE pemesanan_header.kd_user ='".@$_SESSION['kd_user']."'";
      // menejemen pagination untuk url sesuai kondisi diatas
      $select_dt_cari = "?page=Pemesanan&halaman=";

  } 
  // Kondisi where untuk mencari berdasarkan nama
  if(@$_GET['cari']){
    @$dt_cari = "where pemesanan_header.no_pemesanan LIKE '%".@$_GET['cari']."%' and pemesanan_header.kd_user='".@$_SESSION['kd_user']."'";
    // menejemen pagination untuk url sesuai kondisi diatas
    $select_dt_cari ='?page=Pemesanan&cari='.@$_GET['cari'].'&halaman=';
  }
  
  $query = mysql_query("SELECT * FROM pemesanan_header
                left join user ON pemesanan_header.kd_user = user.kd_user
                left join kota ON pemesanan_header.id_kota = kota.id_kota
  $dt_cari ORDER BY pemesanan_header.no_pemesanan DESC LIMIT ".$limit_start.",".$limit);
  
  $no = $limit_start + 1;
  while($data = mysql_fetch_array($query)){ 
                             ?>
            <tr>
              <th class="text-center align-middle" scope="row"><?php echo $data['no_pemesanan']; ?></th>
              <td class="text-center align-middle"><?php echo $data['tanggal_pemesanan']; ?></td>
              <td class="text-center align-middle"><?php echo $data['nama']; ?></td>
              <td class="text-center align-middle">
    <?php
                    $test=$data['no_pemesanan'];
                    $qry2=mysql_query("SELECT COUNT(kd_barang) AS b_prod FROM pemesanan_detail where no_pemesanan='$test'");
                    $tampil=mysql_fetch_array($qry2);
                    ?>
                    <?php echo $tampil['b_prod']; ?>
    </td>
              <td class="text-center align-middle">
    <?php
                    $qry3=mysql_query("SELECT SUM(jumlah_item) AS jumlah FROM pemesanan_detail where no_pemesanan='$test'");
                    $tampil3=mysql_fetch_array($qry3);
                    
                    ?>
                    <?php echo $tampil3['jumlah']; ?>
    </td>
              <td class="text-center align-middle"><?php echo rupiah($data['ongkos_kirim']); ?></td>
              <td class="text-center align-middle"><?php echo rupiah($data['total_bayar']); ?></td>
   <?php
					if($data['status_pemesanan']=='Sedang Diproses'){
						$tampil_status='Belum Bayar';
					}else if($data['status_pemesanan']=='Proses Pembayaran'){
						$tampil_status='Sedang Diproses';
					}else if($data['status_pemesanan']=='Lunas'){
						$tampil_status='Sudah Lunas';
					}else if($data['status_pemesanan']=='Sedang Dikirim'){
						$tampil_status='Sedang Dikirim';
					}else if($data['status_pemesanan']=='Telah Diterima'){
						$tampil_status='Telah Diterima';
					}
					?>
              <td class="text-center align-middle"><?php echo warna_status($tampil_status);?></td>
    
    <?php
					if($tampil_status=='Belum Bayar'){
					?>
                    <td class="text-center align-middle"><button class="btn btn-warning btn-sm modal_bayar" type="button" id="<?php echo $data['no_pemesanan'] ?>">Bayar</button></td>
					<?php  } ?>
					
					<?php
					if($tampil_status=='Sedang Diproses' || $tampil_status=='Sudah Lunas' || $tampil_status=='Sedang Dikirim'){
					?>
                    <td class="text-center align-middle"><button class="btn btn-warning btn-sm modal_bayar disabled" type="button" id="<?php echo $data['no_pemesanan'] ?>">Bayar</button></td>
					<?php  } ?>
					
					<?php
					if($tampil_status=='Telah Diterima'){
					?>
                    <td class="text-center align-middle">-</button></td>
					<?php  } ?>
    
    
            </tr>
    <?php $no++; } ?>
    <?php
    $qry1= mysql_query("SELECT COUNT(*) jumlah FROM pemesanan_header
    left join user ON pemesanan_header.kd_user = user.kd_user
    left join kota ON pemesanan_header.id_kota = kota.id_kota
$dt_cari");
    $dt_pemesanan = mysql_fetch_array($qry1);
    //JIKA DATA TIDAK ADA
     if($dt_pemesanan['jumlah'] == 0){
    ?>
    <tr>
    <td colspan="99" class="text-center text-lg text-medium">Data Pemesanan Tidak Tersedia.</td>
    </tr>
  <?php } ?>
              </table>
            </div>
			<!-- Pagination-->
      <nav class="pagination">
              <div class="column">
                <ul class="pages">
    <?php
													// Buat query untuk menghitung semua jumlah data
		$sql2 = mysql_query("SELECT COUNT(*) AS jumlah FROM pemesanan_header
    left join user ON pemesanan_header.kd_user = user.kd_user
    left join kota ON pemesanan_header.id_kota = kota.id_kota $dt_cari");
		$get_jumlah = mysql_fetch_array($sql2);

		$jumlah_page = ceil($get_jumlah['jumlah'] / $limit); // Hitung jumlah halamannya
		$jumlah_number = 1; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
		$start_number = ($page > $jumlah_number)? $page - $jumlah_number : 1; // Untuk awal link number
		$end_number = ($page < ($jumlah_page - $jumlah_number))? $page + $jumlah_number : $jumlah_page; // Untuk akhir link number

    for($i = $start_number; $i <= $end_number; $i++)
    {

        $link_active = ($page == $i)? ' class="active"' : '';
    ?>
          <li <?php echo $link_active; ?>><a href="<?php echo $select_dt_cari; echo $i; ?>"><?php echo $i; ?></a></li>
    <?php
    }
    ?>
                </ul>
              </div>

              <!-- LINK NEXT  -->
	<?php
		// Jika page sama dengan jumlah page, maka disable link NEXT nya
		// Artinya page tersebut adalah page terakhir 
		if($page == $jumlah_page || $jumlah_page == 0){ // Jika page terakhir
	?>
		<div onclick="return alert('Ini sudah halaman terakhir')" class="column text-right hidden-xs-down"><a class="btn btn-outline-secondary btn-sm" href="#">Next&nbsp;<i class="icon-arrow-right"></i></a></div>
	<?php
		}else{ // Jika Bukan page terakhir
      $link_next = ($page < $jumlah_page)? $page + 1 : $jumlah_page;
      // if($page < $jumlah_page){
      //   $link_next = $page + 1;
      // }else{
      //   $link_next = $jumlah_page;
      // }
	?>
  <div class="column text-right hidden-xs-down"><a class="btn btn-outline-secondary btn-sm" href="<?php echo $select_dt_cari; echo $link_next; ?>">Next&nbsp;<i class="icon-arrow-right"></i></a></div>

	<?php
		}	
		?>
              
            </nav>
          </div>

          <!-- END PAGINATION -->
			
          </div>
          <!-- Sidebar          -->
         
        </div>
      </div>
      
    </div>