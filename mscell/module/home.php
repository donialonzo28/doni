 <div class="offcanvas-wrapper">
      <!-- Page Content-->
      <!-- Main Slider-->
      <section class="hero-slider" style="background-image: url(img/hero-slider/main-bg.jpg);">
        <div class="owl-carousel large-controls dots-inside" data-owl-carousel="{ &quot;nav&quot;: true, &quot;dots&quot;: true, &quot;loop&quot;: true, &quot;autoplay&quot;: true, &quot;autoplayTimeout&quot;: 7000 }">
        <?php
          $qry=mysql_query("SELECT * FROM `barang` ORDER BY kd_barang DESC LIMIT 10");
          while ($tampil=mysql_fetch_array($qry)){ ?>
  
          <div class="item">
            <div class="container padding-top-3x">
              <div class="row justify-content-center align-items-center">
                <div class="col-lg-5 col-md-6 padding-bottom-2x text-md-left text-center">
                  <div class="from-bottom">
                    <div class="h2 text-body text-normal mb-2 pt-1"><?php echo $tampil['nama_barang']?></div>
                    <div class="h2 text-body text-normal mb-4 pb-1"><span class="text-bold"><?php echo rupiah($tampil['harga'])?></span></div>
                  </div><a class="btn btn-primary scale-up delay-1" href="?page=Detail-Produk&id=<?php echo $tampil['kd_barang'] ?>">Lihat Detail</a>
                </div>
                <div class="col-md-6 padding-bottom-2x mb-3"><img width="400px" height="400px" 
                class="d-block mx-auto" src="foto_barang/<?php echo $tampil['foto']?>"></div>
              </div>
            </div>
          </div>
          <?php } ?>
         
        </div>
      </section>
      <!-- Top Categories-->
      <section class="container padding-top-3x">
        <h3 class="text-center mb-30">Top Categories</h3>
        <div class="row">
        <?php
            //KATEGORI OTOMATIS
            $qry_kategori=mysql_query("SELECT * FROM `kategori` ORDER BY id DESC LIMIT 3");
            while ($kategori=mysql_fetch_array($qry_kategori)){
            $id_kategori =  $kategori['id'];
            $nama_kategori =  $kategori['nama_kategori'];

            
            //ISI KATEGORI
            $qry=mysql_query("SELECT * FROM `barang` 
            WHERE id_kategori='".$id_kategori."'");
            $tampil=mysql_fetch_array($qry);

            //MULAI DARI
            $qry1= mysql_query("SELECT MIN(harga) termurah FROM barang where id_kategori='".$id_kategori."'");
            $dt_mulai = mysql_fetch_array($qry1);
        ?>
          <div class="col-md-4 col-sm-6">
            <div class="card mb-30"><a class="card-img-tiles" href="?page=Detail-Produk&id=<?php echo $tampil['kd_barang'] ?>">
                <div class="inner">
                  <div class="main-img"><img src="foto_barang/<?php echo $tampil['foto']?>" alt="Category"></div>
                
                </div></a>
              <div class="card-body text-center">
                <h4 class="card-title"><?php echo $nama_kategori ?></h4>
                <p class="text-muted">Mulai dari <?php echo rupiah($dt_mulai['termurah']) ?> </p><a class="btn btn-outline-pink btn-sm" href="index.php?page=Produk-2&kategori=<?php echo $id_kategori ?>">Lihat Produk</a>
              </div>
            </div>
          </div>
          <?php
          
        }
        ?>

          
        </div>
        <div class="text-center"><a class="btn btn-outline-secondary margin-top-none" href="index.php?page=Produk-2">Semua Kategori</a></div>
      </section>
      <!-- Promo #1-->
     
      <!-- Promo #2-->
      
      <!-- Featured Products Carousel-->
      
      <!-- Product Widgets-->
      <section class="container padding-top-2x">
        <div class="row">
          <div class="col-md-4 col-sm-6">
            <div class="widget widget-featured-products">
              <h3 class="widget-title">PRODUK TERBARU</h3>
              <?php
          $qry=mysql_query("SELECT * FROM `barang` ORDER BY kd_barang DESC LIMIT 3");
          while ($tampil=mysql_fetch_array($qry)){ ?>
              <!-- Entry-->
              <div class="entry">
                <div class="entry-thumb"><a href="?page=Detail-Produk&id=<?php echo $tampil['kd_barang']?>"><img src="foto_barang/<?php echo $tampil['foto']?>" alt="Product"></a></div>
                <div class="entry-content">
                  <h4 class="entry-title"><a href="?page=Detail-Produk&id=<?php echo $tampil['kd_barang']?>"><?php echo $tampil['nama_barang']?></a></h4><span class="entry-meta"><?php echo rupiah($tampil['harga']) ?></span>
                </div>
              </div>
          <?php } ?>
              
            </div>
          </div>
          <div class="col-md-4 col-sm-6">
            <div class="widget widget-featured-products">
              <h3 class="widget-title">PRODUK TOP</h3>
              <?php
          $qry=mysql_query("SELECT * FROM `barang` ORDER BY kd_barang ASC LIMIT 3");
          while ($tampil=mysql_fetch_array($qry)){ ?>
              <!-- Entry-->
              <div class="entry">
                <div class="entry-thumb"><a href="?page=Detail-Produk&id=<?php echo $tampil['kd_barang']?>"><img src="foto_barang/<?php echo $tampil['foto']?>" alt="Product"></a></div>
                <div class="entry-content">
                  <h4 class="entry-title"><a href="?page=Detail-Produk&id=<?php echo $tampil['kd_barang']?>"><?php echo $tampil['nama_barang']?></a></h4><span class="entry-meta"><?php echo rupiah($tampil['harga']) ?></span>
                </div>
              </div>
          <?php } ?>
              
            </div>
          </div>
          <div class="col-md-4 col-sm-6">
            <div class="widget widget-featured-products">
              <h3 class="widget-title">PRODUK TERBAIK</h3>
              <?php
          $qry=mysql_query("SELECT * FROM `barang` ORDER BY harga ASC LIMIT 3");
          while ($tampil=mysql_fetch_array($qry)){ ?>
              <!-- Entry-->
              <div class="entry">
                <div class="entry-thumb"><a href="?page=Detail-Produk&id=<?php echo $tampil['kd_barang']?>"><img src="foto_barang/<?php echo $tampil['foto']?>" alt="Product"></a></div>
                <div class="entry-content">
                  <h4 class="entry-title"><a href="?page=Detail-Produk&id=<?php echo $tampil['kd_barang']?>"><?php echo $tampil['nama_barang']?></a></h4><span class="entry-meta"><?php echo rupiah($tampil['harga']) ?></span>
                </div>
              </div>
          <?php } ?>
            </div>
          </div>
        </div>
      </section>
      <!-- Popular Brands-->
     
      
      <!-- Site Footer-->
      
    </div>