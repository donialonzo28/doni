<?php 
    if(!sudah_login()){ //Jika belum login maka ya login dulu ya
      echo "<script> alert('Untuk melihat keranjang silahkan login terlebih dahulu');window.location.href='?page=Login'</script>";
    }
?>
<div class="offcanvas-wrapper">
      <!-- Page Title-->
      <div class="page-title">
        <div class="container">
          <div class="column">
            <h1>Data Keranjang</h1>
          </div>
          <div class="column">
            <ul class="breadcrumbs">
              <li><a href="index.html">MS Cell</a>
              </li>
              <li class="separator">&nbsp;</li>
              <li>Data Keranjang</li>
            </ul>
          </div>
        </div>
      </div>
      <!-- Page Content-->
      <div class="container padding-bottom-3x mb-1">
        <!-- Alert-->
		        <div class="alert alert-pink alert-dismissible fade show text-center" style="margin-bottom: 30px;"><span class="alert-close" data-dismiss="alert"></span><img class="d-inline align-center"  width="18" height="18">&nbsp;&nbsp;Mohon dicheck kembali apakah produk yang anda pesan sudah sesuai!</div>

        <!-- Shopping Cart-->
        <div class="table-responsive shopping-cart">
          <table class="table">
            <thead>
              <tr>
                <th>Nama Produk</th>
                <th class="text-center">Quantity</th>
                <th class="text-center">Harga</th>
                <th class="text-center">Sub Total</th>
                <th class="text-center">
                <?php
                    $qry1= mysql_query("SELECT COUNT(*) jumlah FROM pemesanan_temp where kd_user='".@$_SESSION['kd_user']."'");
                    $dt_keranjang = mysql_fetch_array($qry1);

                    if($dt_keranjang['jumlah'] > 0){
                  ?>
                <a class="btn btn-sm btn-outline-danger" href="proses/hapus_semua_keranjang.php?keranjang=<?php echo @$_SESSION['kd_user'] ?>" onclick="return confirm('Apakah ingin menghapus semua item ini dari keranjang?')">Hapus Semua</a></th>
                  <?php } ?>
                </tr>
            </thead>
            <tbody>
            <?php
      $qry=mysql_query("SELECT pemesanan_temp.*, barang.*, kategori.nama_kategori from pemesanan_temp
       left join barang on pemesanan_temp.kd_barang = barang.kd_barang
       left join kategori on barang.id_kategori = kategori.id
      WHERE pemesanan_temp.kd_user = '".@$_SESSION['kd_user']."'");
      while ($tampil=mysql_fetch_array($qry)){
            ?>
              <tr>
                <td>
                  <div class="product-item"><a class="product-thumb" href="?page=Detail-Produk&id=<?php echo $tampil['kd_barang']?>"><img src="foto_barang/<?php echo $tampil['foto']?>" alt="Product"></a>
                    <div class="product-info">
                      <h4 class="product-title"><a href="?page=Detail-Produk&id=<?php echo $tampil['kd_barang']?>"><?php echo $tampil['nama_barang']?></a></h4><span><em>Kategori : </em><?php echo $tampil['nama_kategori']?></span>
                    </div>
                  </div>
                </td>
				        <!-- <td class="text-center">
                  <div class="count-input">
                    <select onChange="window.document.location.href=this.options[this.selectedIndex].value;" class="form-control">
                    
                      <option value="proses/update_ukuran_keranjang.php?id_temp=<?php echo $tampil['id_temp'] ?>&size=S" <?php ($tampil['ukuran_barang'] == 'S') ? $sl='selected' : $sl =''; echo $sl ?>>S</option>
                      <option value="proses/update_ukuran_keranjang.php?id_temp=<?php echo $tampil['id_temp'] ?>&size=M" <?php ($tampil['ukuran_barang'] == 'M') ? $sl='selected' : $sl =''; echo $sl ?>>M</option>
                      <option value="proses/update_ukuran_keranjang.php?id_temp=<?php echo $tampil['id_temp'] ?>&size=L" <?php ($tampil['ukuran_barang'] == 'L') ? $sl='selected' : $sl =''; echo $sl ?>>L</option>
                      <option value="proses/update_ukuran_keranjang.php?id_temp=<?php echo $tampil['id_temp'] ?>&size=XL" <?php ($tampil['ukuran_barang'] == 'XL') ? $sl='selected' : $sl =''; echo $sl ?>>XL</option>
                    </select>
                  </div>
                </td> -->
                <td class="text-center">
                  <div class="count-input">
                    <select onChange="window.document.location.href=this.options[this.selectedIndex].value;" class="form-control">
                      <option value="proses/update_qty_keranjang.php?id_temp=<?php echo $tampil['id_temp'] ?>&qty=1" <?php ($tampil['jumlah_item']==1) ? $sl = 'selected' : $sl =''; echo $sl ?>>1</option>
                      <option value="proses/update_qty_keranjang.php?id_temp=<?php echo $tampil['id_temp'] ?>&qty=2" <?php ($tampil['jumlah_item']==2) ? $sl = 'selected' : $sl =''; echo $sl ?>>2</option>
                      <option value="proses/update_qty_keranjang.php?id_temp=<?php echo $tampil['id_temp'] ?>&qty=3" <?php ($tampil['jumlah_item']==3) ? $sl = 'selected' : $sl =''; echo $sl ?>>3</option>
                      <option value="proses/update_qty_keranjang.php?id_temp=<?php echo $tampil['id_temp'] ?>&qty=4" <?php ($tampil['jumlah_item']==4) ? $sl = 'selected' : $sl =''; echo $sl ?>>4</option>
                      <option value="proses/update_qty_keranjang.php?id_temp=<?php echo $tampil['id_temp'] ?>&qty=5" <?php ($tampil['jumlah_item']==5) ? $sl = 'selected' : $sl =''; echo $sl ?>>5</option>
                      <option value="proses/update_qty_keranjang.php?id_temp=<?php echo $tampil['id_temp'] ?>&qty=6" <?php ($tampil['jumlah_item']==6) ? $sl = 'selected' : $sl =''; echo $sl ?>>6</option>
                      <option value="proses/update_qty_keranjang.php?id_temp=<?php echo $tampil['id_temp'] ?>&qty=7" <?php ($tampil['jumlah_item']==7) ? $sl = 'selected' : $sl =''; echo $sl ?>>7</option>
                      <option value="proses/update_qty_keranjang.php?id_temp=<?php echo $tampil['id_temp'] ?>&qty=8" <?php ($tampil['jumlah_item']==8) ? $sl = 'selected' : $sl =''; echo $sl ?>>8</option>
                    </select>
                  </div>
                </td>
                <td class="text-center text-lg text-medium"><?php echo rupiah($tampil['harga']) ?></td>
                <td class="text-center text-lg text-medium">
                <?php 
                $sub_total= $tampil['harga']*$tampil['jumlah_item'];
                @$total_belanja= @$total_belanja + $sub_total;
                echo rupiah($sub_total);
                ?>
                
                </td>
                <td class="text-center"><a class="remove-from-cart" href="proses/hapus_item_keranjang.php?item=<?php echo $tampil['id_temp'] ?>" data-toggle="tooltip" title="Hapus Item" onclick="return confirm('Apakah ingin menghapus item ini dari keranjang?')"><i class="icon-cross"></i></a>
                </td>
               </tr>
              <?php } ?>
              <?php
        if($dt_keranjang['jumlah'] == 0){
      ?>
        <tr>
          <td colspan="99" class="text-center text-lg text-medium">Keranjang masih kosong.</td>
        </tr>
        <?php } ?>
            </tbody>
          </table>
        </div>
        <?php
        if($dt_keranjang['jumlah'] > 0){
        ?>
        <div class="shopping-cart-footer">
          <div class="column text-lg">Total Belanja: <span class="text-medium"><?php echo rupiah(@$total_belanja); ?></span></div>
        </div>
        <?php } ?>
        <div class="shopping-cart-footer">
          <div class="column"><a class="btn btn-outline-secondary" href="index.php?page=Produk"><i class="icon-arrow-left"></i>&nbsp;Kembali Belanja</a></div>
          <div class="column"><a class="btn btn-primary" href="index.php?page=Selesai-Belanja" >Selesai Belanja</a></div>
        </div>

        
      </div>
     
    </div>