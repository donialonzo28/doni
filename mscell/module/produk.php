<div class="offcanvas-wrapper">
      <!-- Page Title-->
      <div class="page-title">
        <div class="container">
          <div class="column">
            <h1>Data Produk</h1>
          </div>
          <div class="column">
            <ul class="breadcrumbs">
              <li><a href="index.html">MS Cell</a>
              </li>
              <li class="separator">&nbsp;</li>
              <li>Data Produk</li>
            </ul>
          </div>
        </div>
      </div>
      <!-- Page Content-->
      <div class="container padding-bottom-3x mb-1">
        <!-- Shop Toolbar-->
        <div class="shop-toolbar padding-bottom-1x mb-2">
          <div class="column">
            <div class="shop-sorting">
            <label for="sorting">Urutkan Berdasarkan:</label>
                  <select onChange="window.document.location.href=this.options[this.selectedIndex].value;" class="form-control" id="urutkan">
                  <option value="#">Pilih Urutkan </option>
                    <option value="?page=Produk&urutkan=terbaru" <?php (@$_GET['urutkan']=='terbaru') ? $sl = 'selected' : $sl =''; echo $sl ?>>Produk Terbaru</option>
                    <option value="?page=Produk&urutkan=termurah" <?php (@$_GET['urutkan']=='termurah') ? $sl = 'selected' : $sl =''; echo $sl ?>>Harga Termurah</option>
                    <option value="?page=Produk&urutkan=termahal" <?php (@$_GET['urutkan']=='termahal') ? $sl = 'selected' : $sl =''; echo $sl ?>>Harga Termahal</option>
                  </select>
            </div>
          </div>
          <div class="column">
            <div class="shop-view">
			<a class="grid-view active" href="?page=Produk"><span></span><span></span><span></span></a>
			<a class="list-view" href="?page=Produk-2"><span></span><span></span><span></span></a></div>
          </div>
        </div>
        <!-- Products Grid-->
        <div class="isotope-grid cols-4 mb-2">
          <div class="gutter-sizer"></div>
          <div class="grid-sizer"></div>
          <?php

if(!@$_GET['urutkan']){
  @$dt_urutkan = "";
  // menejemen pagination untuk url sesuai kondisi diatas
  $select_dt_urutkan = "?page=Produk&halaman=";

} 
// Kondisi where untuk menurutkan berdasarkan nama
if(@$_GET['urutkan']){
  if($_GET['urutkan'] == 'termurah'){
    @$dt_urutkan = "ORDER BY harga ASC";
  }else if($_GET['urutkan'] == 'termahal'){
    @$dt_urutkan = "ORDER BY harga DESC";
  }else if($_GET['urutkan'] == 'terbaru'){
    @$dt_urutkan = "ORDER BY kd_barang DESC";
  }
  // menejemen pagination untuk url sesuai kondisi diatas
  $select_dt_urutkan ='?page=Produk&urutkan='.@$_GET['urutkan'].'&halaman=';
}
    //LIMIT HALAMAN
		$page = (isset($_GET['halaman']))? $_GET['halaman'] : 1;
    $limit = 6; 
    $limit_start = ($page - 1) * $limit;

		$qry=mysql_query("SELECT * FROM `barang` $dt_urutkan LIMIT ".$limit_start.",".$limit);
		while ($tampil=mysql_fetch_array($qry)){ ?>
          <!-- Product-->
          <div class="grid-item">
		   <!-- tambahkan heigt="388px" pada product card-->
            <div class="product-card"><a class="product-thumb" href="?page=Detail-Produk&id=<?php echo $tampil['kd_barang'] ?>"><img src="foto_barang/<?php echo $tampil['foto'] ?>" alt="Product"></a>
              <h3 class="product-title"><a href="?page=Detail-Produk&id=<?php echo $tampil['kd_barang'] ?>"><?php echo $tampil['nama_barang']; ?></a></h3>
              <h4 class="product-price"><?php echo rupiah($tampil['harga']) ?></h4>
              <div class="product-buttons">
           <a class="btn btn-outline-primary btn-sm"
                href="proses/tambah_item_keranjang.php?id=<?php echo $tampil['kd_barang']; ?>">Tambahkan Ke Keranjang</a>
              </div>
            </div>
          </div>
          
        <?php } ?>
        </div>
        <!-- Pagination-->
        <nav class="pagination">
          <div class="column">
            <ul class="pages">
            <?php
													// Buat query untuk menghitung semua jumlah data
		$sql2 = mysql_query("SELECT COUNT(*) AS jumlah from `barang` $dt_urutkan");
		$get_jumlah = mysql_fetch_array($sql2);

		$jumlah_page = ceil($get_jumlah['jumlah'] / $limit); // Hitung jumlah halamannya
		$jumlah_number = 1; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
		$start_number = ($page > $jumlah_number)? $page - $jumlah_number : 1; // Untuk awal link number
		$end_number = ($page < ($jumlah_page - $jumlah_number))? $page + $jumlah_number : $jumlah_page; // Untuk akhir link number

    for($i = $start_number; $i <= $end_number; $i++)
    {

        $link_active = ($page == $i)? ' class="active"' : '';
    ?>
          <li <?php echo $link_active; ?>><a href="<?php echo $select_dt_urutkan; echo $i; ?>"><?php echo $i; ?></a></li>
    <?php
    }
    ?>
                </ul>
              </div>

                 <!-- LINK NEXT  -->
	<?php
		// Jika page sama dengan jumlah page, maka disable link NEXT nya
		// Artinya page tersebut adalah page terakhir 
		if($page == $jumlah_page){ // Jika page terakhir
	?>
		<div class="column text-right hidden-xs-down"><a onclick="return alert('Ini sudah halaman terakhir')" class="btn btn-outline-secondary btn-sm" href="#">Next&nbsp;<i class="icon-arrow-right"></i></a></div>
	<?php
		}else{ // Jika Bukan page terakhir
			$link_next = ($page < $jumlah_page)? $page + 1 : $jumlah_page;
	?>
  <div class="column text-right hidden-xs-down"><a class="btn btn-outline-secondary btn-sm" href="<?php echo $select_dt_urutkan; echo $link_next; ?>">Next&nbsp;<i class="icon-arrow-right"></i></a></div>

	<?php
		}	
		?></nav>
      </div>
     
    </div>