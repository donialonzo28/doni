<?php
// Load file koneksi.php
session_start();
    require_once ('../config/koneksi-mysqli.php');

// Ambil Data yang Dikirim dari Form
$kd_barang = $_POST['kd_barang'];
$nama_barang = $_POST['nama_barang'];
$deskripsi = $_POST['deskripsi'];
$id_kategori = $_POST['id_kategori'];
$harga = $_POST['harga'];

// Cek apakah user ingin mengubah fotonya atau tidak
if(isset($_POST['ubah_foto'])){ // Jika user menceklis checkbox yang ada di form ubah, lakukan :
	// Ambil data foto yang dipilih dari form
	$foto = $_FILES['foto']['name'];
	$tmp = $_FILES['foto']['tmp_name'];
	
	// Rename nama fotonya dengan menambahkan tanggal dan jam upload
	$fotobaru = $foto;
	
	// Set path folder tempat menyimpan fotonya
	$path = "../foto_barang/".$fotobaru;

	// Proses upload
	if(move_uploaded_file($tmp, $path)){ // Cek apakah gambar berhasil diupload atau tidak
		// Query untuk menampilkan data barang berdasarkan kd_barang yang dikirim
		$query = "SELECT * FROM barang WHERE kd_barang='".$kd_barang."'";
		$sql = mysqli_query($connect, $query); // Eksekusi/Jalankan query dari variabel $query
		$data = mysqli_fetch_array($sql); // Ambil data dari hasil eksekusi $sql

		// Cek apakah file foto sebelumnya ada di folder images
		if(is_file("../foto/".$data['foto'])) // Jika foto ada
			unlink("../foto/".$data['foto']); // Hapus file foto sebelumnya yang ada di folder images
		
		// Proses ubah data ke Database
		$query = "UPDATE barang SET nama_barang='".$nama_barang."', deskripsi='".$deskripsi."', id_kategori='".$id_kategori."', harga='".$harga."', foto='".$fotobaru."' WHERE kd_barang='".$kd_barang."'";
		$sql = mysqli_query($connect, $query); // Eksekusi/ Jalankan query dari variabel $query

		if($sql){ // Cek jika proses simpan ke database sukses atau tidak
			// Jika Sukses, Lakukan :
			echo "<script> alert('Data barang berhasil di ubah');window.location.href='view.php?page=Produk'</script>";
		}else{
			// Jika Gagal, Lakukan :
			echo "<script> alert('Data barang gagal di ubah');window.location.href='view.php?page=Produk'</script>";
		}
	}else{
		// Jika gambar gagal diupload, Lakukan :
		echo"<script> alert('Maaf, Gambar gagal untuk diupload.h');window.location.href='view.php?page=Produk'</script>";
	}
}else{ // Jika user tidak menceklis checkbox yang ada di form ubah, lakukan :
	// Proses ubah data ke Database
	$query = "UPDATE barang SET nama_barang='".$nama_barang."', deskripsi='".$deskripsi."', id_kategori='".$id_kategori."', harga='".$harga."' WHERE kd_barang='".$kd_barang."'";
	$sql = mysqli_query($connect, $query); 

	if($sql){ // Cek jika proses simpan ke database sukses atau tidak
		// Jika Sukses, Lakukan :
		echo "<script> alert('Data barang berhasil di ubah');window.location.href='view.php?page=Produk'</script>";
	}else{
		// Jika Gagal, Lakukan :
		echo "<script> alert('Data barang gagal di ubah');window.location.href='view.php?page=Produk'</script>";
	}
}
?>