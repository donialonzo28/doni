<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="img/user.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $_SESSION['nama']; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
     
	 <?php 
						  if(@$_GET['page'] == 'Beranda' ||@$_GET['page'] == ''){
							  $nav_active_beranda= 'class="active"';
						  }else if (@$_GET['page'] == 'Produk') {
							  $nav_active_produk= 'class="active"';
						  }else if (@$_GET['page'] == 'Stok') {
                $nav_active_stok= 'class="active"';
              }else if (@$_GET['page'] == 'Konfirmasi-Pembayaran'){
							  $menu='active';
							  $nav_active_konfir = 'class="active"';
						  }else if (@$_GET['page'] == 'Pengiriman'){
							  $menu='active';
							  $nav_active_pengiriman = 'class="active"';
						  }else if (@$_GET['page'] == 'Laporan') {
							  $nav_active_laporan= 'class="active"';
						  }else if (@$_GET['page'] == 'Data-User'){
							  $menu2='active';
							  $nav_active_user = 'class="active"';
						  }else if (@$_GET['page'] == 'Jasa-Pengiriman'){
							  $menu2='active';
							  $nav_active_jasa_pengiriman = 'class="active"';
						  }else if (@$_GET['page'] == 'Kota'){
							  $menu2='active';
							  $nav_active_kota = 'class="active"';
						  }else if (@$_GET['page'] == 'Kategori'){
                $menu2='active';
                $nav_active_kategori = 'class="active"';
              }
						  ?>
	 
	 
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
		<!-- menu nav -->
		<li <?php echo $nav_active_beranda; ?>>
          <a href="?page=Beranda">
            <i class="fa fa-home"></i> <span>Beranda</span>
          </a>
        </li>
		
		

		<li <?php echo $nav_active_produk; ?>>
          <a href="?page=Produk">
            <i class="fa fa-file-text"></i> <span>Produk</span>
          </a>
        </li>

    <li <?php echo $nav_active_stok; ?>>
          <a href="?page=Stok">
            <i class="fa fa-file-text"></i> <span>Stok</span>
          </a>
        </li>
		
		<!--<li class="">
          <a href="?page=Transaksi">
            <i class="fa fa-check-square"></i> <span>Transaksi</span>
          </a>
        </li>-->
		
		<li class="treeview <?php echo $menu; ?>">
          <a href="#">
            <i class="fa fa-check-square"></i> <span>Transaksi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php echo $nav_active_konfir; ?>><a href="?page=Konfirmasi-Pembayaran"><i class="fa fa-circle-o"></i> Konfirmasi Pembayaran</a></li>
            <li <?php echo $nav_active_pengiriman; ?>><a href="?page=Pengiriman"><i class="fa fa-circle-o"></i> Pengiriman</a></li>
          </ul>
        </li>
		
		<li <?php echo $nav_active_laporan; ?>>
          <a href="?page=Laporan <?php echo $menu2; ?>">
            <i class="fa fa-book"></i> <span>Laporan</span>
          </a>
        </li>
		 <li class="treeview <?php echo $menu2; ?>">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php echo $nav_active_user; ?>><a href="?page=Data-User"><i class="fa fa-circle-o"></i> User</a></li>
            <li <?php echo $nav_active_jasa_pengiriman; ?>><a href="?page=Jasa-Pengiriman"><i class="fa fa-circle-o"></i> Jasa Pengiriman</a></li>
            <li <?php echo $nav_active_kota; ?>><a href="?page=Kota"><i class="fa fa-circle-o"></i> Kota</a></li>
            <li <?php echo $nav_active_kategori; ?>><a href="?page=Kategori"><i class="fa fa-circle-o"></i> Kategori</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>