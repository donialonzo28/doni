<?php
include "../config/inc.connection.php";
include "../config/my_function.php";
?>

<section class="content-header">
      <h1>
        Laporan Pembayaran
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Laporan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
	  
	 
	  
      <!-- Main row -->
      <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Data Laporan Pembayaran</h3>
            </div>
            <form style="margin: 10px;">
            	<?php
            	function ubah_tgl($tanggal) { 
				   $pisah   = explode('-',$tanggal);
				   $larik   = array($pisah[1],$pisah[2],$pisah[0]);
				   $satukan = implode('/',$larik);
				   return $satukan;
				}
				function ubah_tgl2($tanggal) { 
				   $pisah   = explode('/',$tanggal);
				   $larik   = array($pisah[2],$pisah[0],$pisah[1]);
				   $satukan = implode('-',$larik);
				   return $satukan;
				}
            	$tanggal = isset($_GET['tanggal']) ? $_GET['tanggal'] : date('m/d/Y - m/d/Y');
            	$tgl = explode(' - ', $tanggal);
            	$tgl1 = ubah_tgl2($tgl[0]);
            	$tgl2 = ubah_tgl2($tgl[1]);
            	?>
            	<table>
            		<tr>
            			<td>
							<label>Tanggal Pemesanan</label>
            			</td>
            			<td>
            				<input type="hidden" name="page" value="Laporan">
							<input type="text" id="daterangepicker" name="tanggal" value="<?php echo $tanggal ?>" class="form-control" style="margin-left: 15px;max-width: 300px;">
            			</td>
            			<td>
            				<button type="submit" class="btn btn-success">Cari</button>
            			</td>
            		</tr>
            	</table>
			</form>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered">
                <thead>
                <tr>
                  <th class="text-center">No Pemesanan</th>
                  <th class="text-center">Nama Pelanggan</th>
                  <th class="text-center">Item</th>
                  <th class="text-center">Jumlah Barang</th>
                  <th class="text-center">Ongkir</th>
                  <th class="text-center">Sub Total</th>
                  <th class="text-center">Status</th>
                  <th class="text-center">Aksi</th>
                </tr>
                </thead>
                <tbody>
											<?php 
											
											$query=mysql_query("SELECT * FROM pemesanan_header
											left join user ON pemesanan_header.kd_user = user.kd_user
											left join kota ON pemesanan_header.id_kota = kota.id_kota
											where pemesanan_header.status_pemesanan IN('Lunas', 'Sedang Dikirim', 'Telah Diterima') AND pemesanan_header.tanggal_pemesanan >= '$tgl1 00:00:00' AND pemesanan_header.tanggal_pemesanan <= '$tgl2 59:59:59'");
											while($data=mysql_fetch_array($query)){
											
											?>
											  <tr>
													<td class="text-center"><?php echo $data['no_pemesanan'];?></td>
													<td class="text-center"><?php echo $data['nama'];?></td>
													<td class="text-center">
													<?php
													$test=$data['no_pemesanan'];
													$qry2=mysql_query("SELECT COUNT(kd_barang) AS b_prod FROM pemesanan_detail where no_pemesanan='$test'");
													$tampil=mysql_fetch_array($qry2);
													?>
													<?php echo $tampil['b_prod']; ?>
													
													
													</td>
													
													<td class="text-center">
													<?php
													$qry3=mysql_query("SELECT SUM(jumlah_item) AS jumlah FROM pemesanan_detail where no_pemesanan='$test'");
													$tampil3=mysql_fetch_array($qry3);
													
													?>
													<?php echo $tampil3['jumlah']; ?>
													</td>
													<td class="text-center"><?php echo rupiah($data['ongkos_kirim']);?></td>
													<td class="text-center"><?php echo rupiah($data['total_bayar']);?></td>
													<?php
													if($data['status_pemesanan']=='Sedang Diproses'){
														$tampil_status='Belum Bayar';
													}else if($data['status_pemesanan']=='Proses Pembayaran'){
														$tampil_status='Sedang Diproses';
													}else if($data['status_pemesanan']=='Lunas'){
														$tampil_status='Sudah Lunas';
													}else if($data['status_pemesanan']=='Sedang Dikirim'){
														$tampil_status='Sedang Dikirim';
													}else if($data['status_pemesanan']=='Telah Diterima'){
														$tampil_status='Telah Diterima';
													}
													?>
													<td class="text-center"><?php echo warna_status($tampil_status);?></td>
													<td class="text-center">
													<a href="#" class="btn btn-info modal_konfirmasi2" id="<?php echo $data['no_pemesanan'] ?>">Detail</a>
													</td>
											  </tr>
											<?php } ?>
											</tbody>
                <tfoot>
                <tr>
                  <th class="text-center">No Pemesanan</th>
                  <th class="text-center">Nama Pelanggan</th>
                  <th class="text-center">Item</th>
                  <th class="text-center">Jumlah Barang</th>
                  <th class="text-center">Ongkir</th>
                  <th class="text-center">Sub Total</th>
                  <th class="text-center">Status</th>
                  <th class="text-center">Aksi</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
		  
      <!-- /.row (main row) -->
	  
	  
	  <div class="box collapsed-box" style="border-top: 0px;">
            <div class="box-header with-border">
			<?php 
			$get_hitung=mysql_query("SELECT SUM(total_bayar) AS jumlah FROM pemesanan_header;");
			$tampil_hitung=mysql_fetch_array($get_hitung);
			?>
              <small><b>Total Penjualan : <?php echo rupiah($tampil_hitung['jumlah']); ?></b></small>
				
             
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
			
            
			
            <!-- /.box-body -->
			
          </div>

    </section>