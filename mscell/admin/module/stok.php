<?php
    include "../config/inc.connection.php";
?>	

<section class="content-header">
      <h1>
        stok
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">stok</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
	  <form action="proses_tambah_stok.php" method="POST" enctype="multipart/form-data">
	  <div class="box box-default collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Form Tambah Data stok</h3>
				
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
			
            <div class="box-body">
			
              
			   
			  
              <div class="row">
                <div class="col-xs-6">
                  <div class="form-group">
                    <label>Barang</label>
                    <select name="kd_barang" class="form-control" required>
                      <option value="">Pilih Barang</option>
                    <?php
                    $query = "SELECT * FROM barang";
                    $hasil = mysql_query($query);
                    while($data  = mysql_fetch_array($hasil)){
                    ?>
                      <option value="<?php echo $data['kd_barang'] ?>"><?php echo $data['nama_barang'] ?></option>
                    <?php
                    }
                    ?>
                    </select>
                  </div>
                </div>
                <div class="col-xs-6">
                  <div class="form-group">
                    <label>Jumlah Item</label>
                    <input type="text" name="jumlah_item" class="form-control" required onKeyPress="return hanyaAngka(event)">
                  </div>
                </div>
                <div class="col-xs-6">
                  <label>Tanggal Pembelian</label>
                  <div class="form-group input-group">
                    <input type="date" class="form-control" name="tgl_pembelian" value="<?php echo date('Y-m-d') ?>" style="width: max-content;" required>
                    <input type="time" step="2" class="form-control" name="jam_pembelian" value="<?php echo date('H:i:s') ?>" style="width: max-content;" required>
                  </div>
                </div>
                <div class="col-xs-6">
                  <label>Tanggal Penerimaan</label>
                  <div class="form-group input-group">
                    <input type="date" class="form-control" name="tgl_penerimaan" value="<?php echo date('Y-m-d') ?>" style="width: max-content;" required>
                    <input type="time" step="2" class="form-control" name="jam_penerimaan" value="<?php echo date('H:i:s') ?>" style="width: max-content;" required>
                  </div>
                </div>
              </div>
			  
            </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            <!-- /.box-body -->
			
          </div>
		  </form>
	  
	  
	  
	  
      <!-- Main row -->
      <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Data stok</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered">
                <thead>
                <tr>
                  <th class="text-center">No</th>
                  <th class="text-left">Nama Barang</th>
                  <th class="text-center">Tgl. Pembelian</th>
                  <th class="text-center">Tgl. Penerimaan</th>
                  <th class="text-right">Jumlah Item</th>
                  <th class="text-center">Aksi</th>
                </tr>
                </thead>
                <tbody>
				<?php 
											include '../config/Koneksi2.php';		
											include "../config/my_function.php";
											
											$query=mysql_query("SELECT stok.*, barang.nama_barang FROM stok inner join barang on barang.kd_barang = stok.kd_barang");
											$no = 1;
											while($data=mysql_fetch_array($query)){
											
											?>
                <tr>
													<td class="text-center" style="vertical-align: middle; width: 2%;"><?php echo $no; ?></td>
													<td class="text-left" style="vertical-align: middle;"><?php echo $data['nama_barang']; ?></td>
                          <td class="text-center" style="vertical-align: middle;"><?php echo $data['tgl_pembelian']; ?></td>
                          <td class="text-center" style="vertical-align: middle;"><?php echo $data['tgl_penerimaan']; ?></td>
                          <td class="text-right" style="vertical-align: middle;"><?php echo $data['jumlah_item']; ?></td>
													<td style="width: 20%;">
																		<div align="center">
																		<a href="#" class="btn btn-success ubah_stok" id="<?php echo $data['id_stok'] ?>">Ubah</a>
																		<a href="#" class="btn btn-danger hapus_stok" id="<?php echo $data['id_stok'] ?>">Hapus</a>
																		</div>
													</td>
                </tr>
                </tbody>
				<?php $no++; } ?>
                <tfoot>
                <tr>
                  <th class="text-center">No</th>
                  <th class="text-left">Nama Barang</th>
                  <th class="text-center">Tgl. Pembelian</th>
                  <th class="text-center">Tgl. Penerimaan</th>
                  <th class="text-right">Jumlah Item</th>
                  <th class="text-center">Aksi</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      <!-- /.row (main row) -->

    </section>