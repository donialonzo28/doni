<?php
include "../config/inc.connection.php";
include "../config/my_function.php";

$ambil=mysql_query("SELECT COUNT(kd_user) AS jumlah FROM user where level='Pelanggan'");
$tampilkan=mysql_fetch_array($ambil);

$ambil2=mysql_query("SELECT COUNT(no_pemesanan) AS jumlah FROM pemesanan_header where status_pemesanan IN('Sedang Diproses','Proses Pembayaran')");
$tampilkan2=mysql_fetch_array($ambil2);

$ambil3=mysql_query("SELECT COUNT(no_pemesanan) AS jumlah FROM pemesanan_header where status_pemesanan IN ('Lunas', 'Sedang Dikirim', 'Telah Diterima')");
$tampilkan3=mysql_fetch_array($ambil3);

$ambil4=mysql_query("SELECT COUNT(no_pemesanan) AS jumlah FROM pemesanan_header");
$tampilkan4=mysql_fetch_array($ambil4);
?>

<section class="content-header">
      <h1>
        Beranda
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Beranda</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $tampilkan['jumlah']?></h3>

              <p>&nbsp;</p>
            </div>
            <div class="icon">
              <i class="fa fa-user"></i>
            </div>
            <a href="#" class="small-box-footer">Jumlah Konsumen </a>
          </div>
        </div>
		
		<div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $tampilkan2['jumlah']?></h3>

              <p>&nbsp;</p>
            </div>
            <div class="icon">
              <i class="fa fa-fw fa-cloud-download"></i>
            </div>
            <a href="#" class="small-box-footer">Order Belum Dikonfirmasi</a>
          </div>
        </div>
		
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $tampilkan3['jumlah']?></h3>

              <p>&nbsp;</p>
            </div>
            <div class="icon">
              <i class="fa fa-fw fa-check"></i>
            </div>
            <a href="#" class="small-box-footer">Order Telah Dikonfirmasi</a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $tampilkan4['jumlah']?></h3>

              <p>&nbsp;</p>
            </div>
            <div class="icon">
              <i class="fa fa-fw fa-list-alt"></i>
            </div>
            <a href="#" class="small-box-footer">Total Order</a>
          </div>
        </div>
        <!-- ./col -->
        
        <!-- ./col -->
      </div>
      <!-- /.row -->
	  
	  
	  
      <!-- Main row -->
      <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Data Order</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered">
                <thead>
                <tr>
                  <th class="text-center">No Pemesanan</th>
                  <th class="text-center">Nama Konsumen</th>
                  <th class="text-center">Jumlah Produk</th>
                  <th class="text-center">Jumlah Barang</th>
                  <th class="text-center">Status</th>
                </tr>
                </thead>
                <tbody>
				<?php 
											
											$query=mysql_query("SELECT * FROM pemesanan_header
											left join user ON pemesanan_header.kd_user = user.kd_user
											left join kota ON pemesanan_header.id_kota = kota.id_kota");
											while($data=mysql_fetch_array($query)){
											
											?>
                <tr>
                  <td class="text-center"> <?php echo $data['no_pemesanan'];?></td>
											<td class="text-center"> <?php echo $data['nama']; ?>
                 <td class="text-center">
													<?php
													$test=$data['no_pemesanan'];
													$qry2=mysql_query("SELECT COUNT(kd_barang) AS b_prod FROM pemesanan_detail where no_pemesanan='$test'");
													$tampil=mysql_fetch_array($qry2);
													?>
													<?php echo $tampil['b_prod']; ?>
													
													
													</td>
													
													<td class="text-center">
													<?php
													$qry3=mysql_query("SELECT SUM(jumlah_item) AS jumlah FROM pemesanan_detail where no_pemesanan='$test'");
													$tampil3=mysql_fetch_array($qry3);
													
													?>
													<?php echo $tampil3['jumlah']; ?>
													</td>
													
													<?php
													if($data['status_pemesanan']=='Sedang Diproses'){
														$tampil_status='Belum Bayar';
													}else if($data['status_pemesanan']=='Proses Pembayaran'){
														$tampil_status='Sedang Diproses';
													}else if($data['status_pemesanan']=='Lunas'){
														$tampil_status='Sudah Lunas';
													}else if($data['status_pemesanan']=='Sedang Dikirim'){
														$tampil_status='Sedang Dikirim';
													}else if($data['status_pemesanan']=='Telah Diterima'){
														$tampil_status='Telah Diterima';
													}
													?>
													<td class="text-center"><?php echo warna_status($tampil_status);?></td>
											  </tr>
											<?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th class="text-center">No Pemesanan</th>
                  <th class="text-center">Nama Konsumen</th>
                  <th class="text-center">Jumlah Produk</th>
                  <th class="text-center">Jumlah Barang</th>
                  <th class="text-center">Status</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      <!-- /.row (main row) -->

    </section>