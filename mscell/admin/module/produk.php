<?php
    include "../config/inc.connection.php";
	date_default_timezone_set('Asia/Jakarta');
    $query = "SELECT CONCAT('B',max(kd_barang)) as maxkode FROM (SELECT ABS(substr(kd_barang,2,4)) as kd_barang FROM barang) A";
    $hasil = mysql_query($query);
    $data  = mysql_fetch_array($hasil);
    $kd_barang = $data['maxkode'];
    $noUrut = (int) substr($kd_barang,1,4);
    $noUrut++;
    $char = "B";
    $kd_barang = $char . sprintf("%02s", $noUrut);
    

?>	

<section class="content-header">
      <h1>
        Beranda
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Beranda</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
	  <form action="proses_tambah_barang.php" method="POST" enctype="multipart/form-data">
	  <div class="box box-default collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Form Tambah Data Produk</h3>
				
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
			
            <div class="box-body">
			
              <div class="row">
                <div class="col-xs-2">
                  <div class="form-group">
                  <label>Kode&nbsp;Barang</label>
                  <input type="text" class="form-control" placeholder="" name="kd_barang" value="<?php echo $kd_barang;?>" readonly/>
                </div>
                </div>
				<div class="col-xs-6">
                  <div class="form-group">
                  <label>Nama&nbsp;Barang</label>
                  <input type="text" class="form-control" placeholder="" name="nama_barang">
                </div>
                </div>
				<div class="col-xs-2">
                  <div class="form-group">
                  <label>Jenis&nbsp;Barang</label>
				   <select name="id_kategori" id="id_kategori" class="form-control">
									<?php
									$query = mysql_query("SELECT * FROM kategori");
									while ($row = mysql_fetch_array($query)) {
									?>
										<option value="<?php echo $row['id']; ?>">
											<?php echo $row['nama_kategori']; ?>
										</option>
									<?php
									}
									?>
									</select>
                </div>
                </div>
				<div class="col-xs-2">
                  <div class="form-group">
                  <label>Harga</label>
                  <input type="text" class="form-control" placeholder="" name="harga" onKeyPress="return hanyaAngka(event)">
                </div>
                </div>
              </div>
			   <div class="row">
                <div class="col-xs-12">
                  <div class="form-group">
                  <label>Deskripsi</label>
                  <textarea class="form-control" rows="5" placeholder="" name="deskripsi"></textarea>
                </div>
                </div>
              </div>
			  <div class="row">
                <div class="col-xs-12">
                   <div class="form-group">
                  <label for="exampleInputFile">Foto Barang</label>
                  <input type="file" id="exampleInputFile" name="foto">
                </div>
                </div>
              </div>
            </div>
			<div class="box-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            <!-- /.box-body -->
			
          </div>
		  </form>
	  
	  
	  
	  
      <!-- Main row -->
      <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Data Produk</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered">
                <thead>
                <tr>
                  <th class="text-center">Kode Barang</th>
                  <th class="text-center">Nama Barang</th>
                  <th class="text-center">Jenis Barang</th>
                  <th class="text-center">Harga</th>
                  <th class="text-center">Foto</th>
                  <th class="text-center">Aksi</th>
                </tr>
                </thead>
                <tbody>
				<?php 
											include '../config/Koneksi2.php';		
											include "../config/my_function.php";
											
											$query=mysql_query("SELECT barang.*, kategori.nama_kategori FROM barang left join kategori on kategori.id = barang.id_kategori");
											$no = 1;
											while($data=mysql_fetch_array($query)){
											
											?>
                <tr>
													<td class="text-center" style="vertical-align: middle;"><?php echo $data['kd_barang']; ?></td>
													<td class="text-center" style="vertical-align: middle;"><?php echo $data['nama_barang']; ?></td>
													<td class="text-center" style="vertical-align: middle;"><?php echo $data['nama_kategori']; ?></td>
													<td class="text-center" style="vertical-align: middle;"><?php echo rupiah($data['harga']); ?></td>
													<td class="text-center" style="vertical-align: middle;">
													<div class="baguetteBoxThree gallery">
													<a href="../foto_barang/<?php echo $data['foto'];?>" class="effects">
													<img src="../foto_barang/<?php echo $data['foto'];?>" alt="gambar kosong" style="width: 80px;height: 38px;">
													</a>
													</div>
													</td>
													<td>
																		<div align="center">
																		<a href="#" class="btn btn-success ubah_barang" id="<?php echo $data['kd_barang'] ?>">Ubah</a>
																		<a href="#" class="btn btn-danger hapus_barang" id="<?php echo $data['kd_barang'] ?>">Hapus</a>
																		</div>
													</td>
                </tr>
                </tbody>
				<?php $no++; } ?>
                <tfoot>
                <tr>
                  <th class="text-center">Kode Barang</th>
                  <th class="text-center">Nama Barang</th>
                  <th class="text-center">Jensi Barang</th>
                  <th class="text-center">Harga</th>
                  <th class="text-center">Foto</th>
                  <th class="text-center">Aksi</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      <!-- /.row (main row) -->

    </section>