<?php
    include "../config/inc.connection.php";

?>	

<section class="content-header">
      <h1>
        kategori
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">kategori</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
	  <form action="proses_tambah_kategori.php" method="POST" enctype="multipart/form-data">
	  <div class="box box-default collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Form Tambah Data kategori</h3>
				
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
			
            <div class="box-body">
			
              
			   
			  
			  <div class="row">
                <div class="col-xs-6">
                  <div class="form-group">
                    <label>Nama kategori</label>
                    <input type="text" class="form-control" placeholder="" name="nama_kategori" value="">
                  </div>
                </div>
              </div>
			  
            </div>
			<div class="box-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            <!-- /.box-body -->
			
          </div>
		  </form>
	  
	  
	  
	  
      <!-- Main row -->
      <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Data kategori</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered">
                <thead>
                <tr>
                  <th class="text-center">No</th>
                  <th class="text-center">Nama kategori</th>
                  <th class="text-center">Aksi</th>
                </tr>
                </thead>
                <tbody>
				<?php 
											include '../config/Koneksi2.php';		
											include "../config/my_function.php";
											
											$query=mysql_query("SELECT * FROM kategori");
											$no = 1;
											while($data=mysql_fetch_array($query)){
											
											?>
                <tr>
													<td class="text-center" style="vertical-align: middle; width: 2%;"><?php echo $no; ?></td>
													<td class="text-left" style="vertical-align: middle;"><?php echo $data['nama_kategori']; ?></td>
													<td style="width: 20%;">
																		<div align="center">
																		<a href="#" class="btn btn-success ubah_kategori" id="<?php echo $data['id'] ?>">Ubah</a>
																		<a href="#" class="btn btn-danger hapus_kategori" id="<?php echo $data['id'] ?>">Hapus</a>
																		</div>
													</td>
                </tr>
                </tbody>
				<?php $no++; } ?>
                <tfoot>
                <tr>
                   <th class="text-center">No</th>
                  <th class="text-center">Nama Kategori</th>
                  <th class="text-center">Aksi</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      <!-- /.row (main row) -->

    </section>