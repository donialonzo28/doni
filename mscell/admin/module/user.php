<?php
    include "../config/inc.connection.php";
    //kduser
	$query = "SELECT CONCAT('U',max(kd_user)) as maxkode FROM (SELECT ABS(substr(kd_user,2,4)) as kd_user FROM user) B";
    $hasil = mysql_query($query);
    $data  = mysql_fetch_array($hasil);
    $kd_user = $data['maxkode'];
    $noUrut = (int) substr($kd_user,1,4);
    $noUrut++;
    $char = "U";
    $kd_user = $char . sprintf("%02s", $noUrut);

?>	

<section class="content-header">
      <h1>
        Data User
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
	  <form action="proses_tambah_user.php" method="POST" enctype="multipart/form-data">
	  <div class="box box-default collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Form Tambah User</h3>
				
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
			
            <div class="box-body">
			
              <div class="row">
			  <div class="col-xs-2">
                  <div class="form-group">
                  <label>Kode User</label>
                  <input type="text" class="form-control" placeholder="" name="kd_user" value="<?php echo $kd_user; ?>" readonly/>
                </div>
                </div>
                <div class="col-xs-2">
                  <div class="form-group">
                  <label>Username</label>
                  <input type="text" class="form-control" placeholder="" name="username" value="">
                </div>
                </div>
				<div class="col-xs-2">
                  <div class="form-group">
                  <label>Password</label>
                  <input type="password" class="form-control" placeholder="" name="password">
                </div>
                </div>
				<div class="col-xs-6">
                  <div class="form-group">
                  <label>Nama&nbsp;Lengkap</label>
				   <input type="text" class="form-control" placeholder="" name="nama">
                </div>
                </div>
              </div>
			   <div class="row">
                <div class="col-xs-12">
                  <div class="form-group">
                  <label>Alamat</label>
                  <textarea class="form-control" rows="5" placeholder="" name="alamat"></textarea>
                </div>
                </div>
              </div>
			  
			  <div class="row">
                <div class="col-xs-6">
                  <div class="form-group">
                  <label>No HP</label>
                  <input type="text" class="form-control" placeholder="" name="no_hp" value="">
                </div>
                </div>
				<div class="col-xs-6">
                  <div class="form-group">
                  <label>Password</label>
                  <select class="form-control" name="level">
																				<option value=""> -- Pilih Level --</option>
																				<option value="Admin">Admin</option>
																				<option value="Konsumen">Konsumen</option>
																			</select>
                </div>
                </div>
              </div>
			  
            </div>
			<div class="box-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            <!-- /.box-body -->
			
          </div>
		  </form>
	  
	  
	  
	  
      <!-- Main row -->
      <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Data User</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered">
                <thead>
                <tr>
                  <th class="text-center">Kode User</th>
                  <th class="text-center">Nama Lengkap</th>
                  <th class="text-center">Username</th>
                  <th class="text-center">Password</th>
                  <th class="text-center">Alamat</th>
                  <th class="text-center">No HP</th>
                  <th class="text-center">Level</th>
                  <th class="text-center">Aksi</th>
                </tr>
                </thead>
                <tbody>
				<?php 
											include '../config/Koneksi2.php';		
											include "../config/my_function.php";
											
											$query=mysql_query("SELECT * FROM user");
											$no = 1;
											while($data=mysql_fetch_array($query)){
											
											?>
                <tr>
													<td class="text-center" style="vertical-align: middle;"><?php echo $data['kd_user']; ?></td>
													<td class="text-center" style="vertical-align: middle;"><?php echo $data['nama']; ?></td>
													<td class="text-center" style="vertical-align: middle;"><?php echo $data['username']; ?></td>
													<td class="text-center" style="vertical-align: middle;"><?php echo $data['password']; ?></td>
													<td class="text-center" style="vertical-align: middle;"><?php echo $data['alamat']; ?></td>
													<td class="text-center" style="vertical-align: middle;"><?php echo $data['no_hp']; ?></td>
													<td class="text-center" style="vertical-align: middle;"><?php echo $data['level']; ?></td>
													<td>
																		<div align="center">
																		<a href="#" class="btn btn-success ubah_user" id="<?php echo $data['kd_user'] ?>">Ubah</a>
																		<a href="#" class="btn btn-danger hapus_user" id="<?php echo $data['kd_user'] ?>">Hapus</a>
																		</div>
													</td>
                </tr>
                </tbody>
				<?php $no++; } ?>
                <tfoot>
                <tr>
                   <th class="text-center">Kode User</th>
                  <th class="text-center">Nama Lengkap</th>
                  <th class="text-center">Username</th>
                  <th class="text-center">Password</th>
                  <th class="text-center">Alamat</th>
                  <th class="text-center">No HP</th>
                  <th class="text-center">Level</th>
                  <th class="text-center">Aksi</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      <!-- /.row (main row) -->

    </section>