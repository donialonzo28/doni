<?php
include "../config/inc.connection.php";
include "../config/my_function.php";
?>

<section class="content-header">
      <h1>
        Transaksi Pembayaran
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Transaksi</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
	  
	 
	  
      <!-- Main row -->
      <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Data Keranjang</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered">
                <thead>
                <tr>
                  <th class="text-center">No Pemesanan</th>
                  <th class="text-center">Nama Pelanggan</th>
                  <th class="text-center">Item</th>
                  <th class="text-center">Jumlah Barang</th>
                  <th class="text-center">Ongkir</th>
                  <th class="text-center">Sub Total</th>
                  <th class="text-center">Status</th>
                  <th class="text-center">Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php 
											
											$query=mysql_query("SELECT * FROM pemesanan_header
											left join user ON pemesanan_header.kd_user = user.kd_user
											left join kota ON pemesanan_header.id_kota = kota.id_kota
											where pemesanan_header.status_pemesanan='Proses Pembayaran'");
											while($data=mysql_fetch_array($query)){
											
											?>
											  <tr>
													<td class="text-center"><?php echo $data['no_pemesanan'];?></td>
													<td class="text-center"><?php echo $data['nama'];?></td>
													<td class="text-center">
													<?php
													$test=$data['no_pemesanan'];
													$qry2=mysql_query("SELECT COUNT(kd_barang) AS b_prod FROM pemesanan_detail where no_pemesanan='$test'");
													$tampil=mysql_fetch_array($qry2);
													?>
													<?php echo $tampil['b_prod']; ?>
													
													
													</td>
													
													<td class="text-center">
													<?php
													$qry3=mysql_query("SELECT SUM(jumlah_item) AS jumlah FROM pemesanan_detail where no_pemesanan='$test'");
													$tampil3=mysql_fetch_array($qry3);
													
													?>
													<?php echo $tampil3['jumlah']; ?>
													</td>
													<td class="text-center"><?php echo rupiah($data['ongkos_kirim']);?></td>
													<td class="text-center"><?php echo rupiah($data['total_bayar']);?></td>
													<?php
													if($data['status_pemesanan']=='Sedang Diproses'){
														$tampil_status='Belum Bayar';
													}else if($data['status_pemesanan']=='Proses Pembayaran'){
														$tampil_status='Sedang Diproses';
													}
													?>
													<td class="text-center"><?php echo warna_status($tampil_status);?></td>
													<td class="text-center">
													<a href="#" class="btn btn-warning modal_konfirmasi" id="<?php echo $data['no_pemesanan'] ?>">Konfirmasi</a>
													</td>
											  </tr>
											<?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th class="text-center">No Pemesanan</th>
                  <th class="text-center">Nama Pelanggan</th>
                  <th class="text-center">Item</th>
                  <th class="text-center">Jumlah Barang</th>
                  <th class="text-center">Ongkir</th>
                  <th class="text-center">Sub Total</th>
                  <th class="text-center">Status</th>
                  <th class="text-center">Aksi</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      <!-- /.row (main row) -->

    </section>